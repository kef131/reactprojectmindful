import React, { Component } from 'react';
import {Error, Success} from '../../Styles/Login/Messages';

export default class FormValidation extends Component
{
    render() {
        const isValidated = this.props.validation;
        const isError = this.props.status;
        const isHidden = this.props.hidden;

        let Message;
        if(isValidated && !isError)
        {
            if(!isHidden)
                Message = <Success><span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> {this.props.message}</Success>

        }
        else
        {
            Message = <Error><span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> {this.props.message}</Error>

        }

        return (
            <div>
                {Message}
            </div>
        );
    }
}