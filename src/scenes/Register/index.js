import React, {Component} from 'react';
import GlobalStyle from '../Styles/Login/GlobalStyle';
import {LoginForm, UserInput, EmailInput, Register, Logotipo, NewPasswordInput, CheckboxAgree} from '../Styles/Login/General';
import {TergarButton} from '../Styles/Login/Buttons';
import {Link} from "react-router-dom";
import Language from '../../components/Language';
import FormValidation from './components/FormValidation';
import validator from 'validator';
import axios from "axios";
import logotipo_tergar from '../../img/logotipo-tergar.svg';

export default class Index extends Component
{
    constructor()
    {
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
            validation: true,
            errorStatus: false,
            hidden: true,
            message: '',
            name: '',
            lastname: '',
            email: '',
            password: '',
            password2: '',
        };

        this.handleName = this.handleName.bind(this);
        this.handleLastName = this.handleLastName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handlePassword2 = this.handlePassword2.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    }

    //Action button submit
    handleSubmit(e)
    {
        e.preventDefault();

        if(!validator.isEmpty(this.state.name) && !validator.isEmpty(this.state.lastname) &&
            !validator.isEmpty(this.state.email) && !validator.isEmpty(this.state.password) && !validator.isEmpty(this.state.password2) && this.state.privacy_policy && this.state.terms_conditions)
        {
            if(validator.isEmail(this.state.email))
            {
                if(!validator.equals(this.state.password,this.state.password2))
                {
                    this.setState({validation: false, message: "The password does not match"});
                    //console.log("Password not match");
                }
                else
                {
                    if((this.state.password.length < 8) && (this.state.password2.length < 8))
                    {
                        this.setState({validation: false, message: "Your password must contain at least 8 characters"});
                    }
                    else
                    {
                        this.setState({validation: true});
                        //Get save data form.
                        const access = {
                            firstName: this.state.name,
                            lastName: this.state.lastname,
                            email: this.state.email,
                            password: this.state.password
                        };

                        //Communication with API.
                        axios.post(`${process.env.API_URL}signup` ,access)
                            .then(response => {

                                //console.log(response);
                                //If it is successful.
                                if(response.data.exito)
                                {
                                    this.setState({errorStatus: false , hidden: true});

                                    axios.post(`${process.env.API_URL}oauth/token`,{username: access.email, password: access.password})
                                        .then( response => {
                                            //console.log(response);
                                                 if("access_token" in  response.data)
                                                 {
                                                     axios.put(`${process.env.API_URL}user/privacyPolicyAndTermsAndConditionsAcceptance`, {oauth_token:response.data.access_token})
                                                         .then( response => {

                                                             //console.log(response);
                                                             if(response.data.exito)
                                                             {
                                                                 this.setState({errorStatus: false , hidden: true});
                                                                 localStorage.setItem("message","Your account has been created!");
                                                                 //Redirect to login
                                                                 window.location = `${process.env.BASE_URL}/login`;
                                                             }else
                                                             {
                                                                 this.setState({errorStatus:true, message: "Error. Please contact to app@tergar.org"});

                                                             }
                                                         })
                                                 }
                                             }
                                        );

                                }else
                                {
                                    this.setState({errorStatus:true, message: "This email is already registered."});
                                    console.log(response.data);
                                }
                            }).catch(() => {
                            this.setState({errorStatus:true, message: "Error Network"});

                        });
                    }
                }
            }
            else
            {
                this.setState({message: 'Email is incorrect'});
            }
        }
        else
        {
            this.setState({validation: false, message: "All the fields are required"});
        }

    }

    // Catch name.
    handleName(e)
    {
        this.setState({name:e.target.value});
    }

    // Catch Last name.
    handleLastName(e)
    {
        this.setState({lastname:e.target.value});
    }

    // Catch Email.
    handleEmail(e)
    {
        this.setState({email:e.target.value});
    }

    // Catch Paasword.
    handlePassword(e)
    {
        this.setState({password:e.target.value});
    }

    // Catch Repeat Password
    handlePassword2(e)
    {
        this.setState({password2:e.target.value});
    }

    handleCheckboxChange(e)
    {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({ [name] : value});
    }

    render() {
        return (
            <LoginForm>
                <div>
                    <form method="POST" >
                        <Logotipo src={logotipo_tergar} alt="Tergar" />
                        <FormValidation message={this.state.translation[this.state.message]} validation={this.state.validation} status={this.state.errorStatus} hidden={this.state.hidden}/>
                        <UserInput type="text" name="name" id="name" placeholder={this.state.translation['Name']} onChange={this.handleName} />
                        <UserInput type="text" name="lastname" id="lastname" placeholder={this.state.translation['Last name']} onChange={this.handleLastName} />
                        <EmailInput type="email" name="email" placeholder={this.state.translation['Email']} onChange={this.handleEmail}/>
                        <NewPasswordInput type="password" name="password" id="password" placeholder={this.state.translation['Password']} onChange={this.handlePassword} />
                        <NewPasswordInput type="password" name="password_confirmation" id="password_confirmation" placeholder={this.state.translation['Repeat Password']} onChange={this.handlePassword2} />
                        <CheckboxAgree>
                            <label><input type="checkbox" name="privacy_policy" id="privacy_policy" onChange={this.handleCheckboxChange} /> {this.state.translation['I agree to the']} <a target="_blank" href="https://www.iubenda.com/privacy-policy/53458676">{this.state.translation['privacy policy']}</a></label>
                            <label ><input type="checkbox" name="terms_conditions" id="terms_conditions" onChange={this.handleCheckboxChange} /> {this.state.translation['I agree to the']} <a target="_blank" href="https://app.tergar.org/terms-conditions-tergar-meditation.pdf">{this.state.translation['terms and conditions']}</a></label>
                        </CheckboxAgree>
                        <TergarButton type="submit" className="iniciar-sesion" onClick={this.handleSubmit}>{this.state.translation['Create Tergar ID']}</TergarButton>
                        <Register><Link to="/login">{this.state.translation['Back to Login']}</Link></Register>
                    </form>
                </div>
                <GlobalStyle/>
            </LoginForm>
        );
    }
}