import styled from 'styled-components';

export const Error = styled.div`
font-size: 13px; 
color:#aa0006; 
border:1px solid #aa0006; 
border-radius: 3px; 
padding: 8px 18px 7px; 
margin-bottom: 10px;list-style: none;

span
{
    font-size: 15px;
}
`

export const Success = styled.div`
    font-size: 13px; 
    color:#037d28; 
    border:1px solid #037d28; 
    border-radius: 3px; 
    padding: 8px 18px 7px; 
    margin-bottom: 10px;
    list-style: none;
`