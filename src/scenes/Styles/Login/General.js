import styled, {css} from 'styled-components';
import user_icon from '../../../img/user-icon.svg';
import pass_icon from '../../../img/pass-icon.svg';
import email_icon from '../../../img/email-icon.svg';
import password_icon from '../../../img/password-icon.svg';

export const LoginForm = styled.div`
    position: relative; 
    display: table; 
    height: 100%; 
    margin: 0 auto; 
    width: 270px;
    
    > div 
    {
        display: table-cell; vertical-align: middle; padding-bottom: 40px;
    }
    
    > div > form > input
    {
        width: 100%; 
        display: block;
    }
   
    > div label
    {
        width: 100%; 
        display: block;
    }
    
    > div input{
        border:none; 
        border-bottom: 1px solid #adadad; 
        background-position: 4px center; 
        padding:10px 10px 8px 26px; 
        background-repeat: no-repeat; 
        box-sizing: border-box; 
        font-size: 14px; 
        color:rgba(48,48,70,0.80)
    }
    
    p
    {
        font-weight: 300;
        font-size: 14px;
    }
`;

export const LoginFormDiv = styled.div`
display: table-cell; 
vertical-align: middle; 
padding-bottom: 40px;
`;

export const UserInput = styled.input`
background-image: url(${user_icon}); 
background-size: 16px;
color: #9a9a9a;
margin-bottom: 20px;
`;

export const PasswordInput = styled.input`
background-image:url(${pass_icon}); 
background-size: 18px;
margin-bottom: 20px;
`;

export const EmailInput = styled.input`
background-image: url(${email_icon}); 
background-size: 16px;
margin-bottom: 20px;
`
export const NewPasswordInput = styled.input`
background-image: url(${password_icon}); 
background-size: 19px;
margin-bottom: 20px;
`
export const Register = styled.p`
font-size: 13px; 
text-align: center; 
color:rgba(48,48,70,0.80); 
padding-bottom: 10px;

a{
font-weight: 500;
}
`

export const Logotipo = styled.img`
width: 140px; 
height: auto; 
margin: 0 auto 20px; 
display: block;
`

export const CheckboxAgree= styled.div`

    label{font-weight:300;font-size:13px;}
    
`

export const Mb20 = css`
    margin-bottom: 20px;
`