import styled from 'styled-components';

export const FBimg = styled.img`
height: 14.6px; 
width: auto; 
display: inline-block; 
margin-top: -3px; 
padding-right: 18px;
`;

const LoginButtons = styled.button`
padding: 10px 15px 8px; 
text-align: center; 
color:#fff; 
text-shadow: 1px 1px 0 rgba(0,0,0,.3); 
text-transform: uppercase; 
box-sizing: border-box; 
display: block; 
width: 100%; 
border: none; 
font-weight: 400; 
font-size: 13px; 
letter-spacing: 1.25px; 
border-radius: 3px; 
margin-bottom: 16px;
cursor: pointer;
 `

export const TergarButton = styled(LoginButtons)`
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fc7f29+0,fa6323+100 */ 
background: #fc7f29; /* Old browsers */ 
background: -moz-linear-gradient(top, #fc7f29 0%, #fa6323 100%); /* FF3.6-15 */ 
background: -webkit-linear-gradient(top, #fc7f29 0%,#fa6323 100%); /* Chrome10-25,Safari5.1-6 */ 
background: linear-gradient(to bottom, #fc7f29 0%,#fa6323 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */ 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fc7f29', endColorstr='#fa6323',GradientType=0 ); /* IE6-9 */

&:hover, &:focus {
    opacity: .9;
}
`

export const FBButton = styled.a`
padding: 10px 15px 8px; 
text-align: center; 
color:#fff; 
text-shadow: 1px 1px 0 rgba(0,0,0,.3); 
text-transform: uppercase; 
box-sizing: border-box; 
display: block; 
width: 100%; 
border: none; 
font-weight: 400; 
font-size: 13px; 
letter-spacing: 1.25px; 
border-radius: 3px; 
margin-bottom: 16px;
cursor: pointer;
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#3368b1+0,395695+100 */
background: #3368b1;
/* Old browsers */
background: -moz-linear-gradient(top, #3368b1 0%, #395695 100%);
/* FF3.6-15 */
background: -webkit-linear-gradient(top, #3368b1 0%,#395695 100%);
/* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #3368b1 0%,#395695 100%);
/* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3368b1', endColorstr='#395695',GradientType=0 ); /* IE6-9 */

&:focus, &:hover {
color:#fff;
opacity: .9;
}

`;