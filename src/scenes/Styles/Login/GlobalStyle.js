import React from 'react';
import {createGlobalStyle} from "styled-components";
import bk_login from '../../../img/bk-login.jpg';

const GlobalStyle = createGlobalStyle`
  @charset "UTF-8";
/* CSS Document */
html,body{width: 100%; height: 100%; margin: 0;font-size:14;}
body{background: url(${bk_login}) bottom center #fff no-repeat; background-attachment: fixed; background-size: 100%; color: #2e2e44; font-size: 15px; font-family: 'Roboto', sans-serif; font-weight: 400;}

.pt6{padding-top: 6px;}
.h100p{height: 100%;}

@media  (max-width: 767px){
	.logotipo-login{width: 110px; margin: 0 auto 10px;}	
	.loginForm > div{padding-bottom: 10px;}
}

@media (max-width: 770px) and (orientation: landscape) {
	body{background: none;}
}
`;

export default GlobalStyle;