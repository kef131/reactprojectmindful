import React, {Component} from 'react';
import GlobalStyle from '../Styles/Login/GlobalStyle';
import {FBButton, FBimg, TergarButton} from "../Styles/Login/Buttons";
import {LoginForm, UserInput,PasswordInput, Register, Logotipo, LoginButton} from '../Styles/Login/General';
import {Link} from "react-router-dom";
import Language from '../../components/Language';
import FormValidation from './components/FormValidation';
import axios from 'axios';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import logotipo_tergar from '../../img/logotipo-tergar.svg';
import facebook_icon from '../../img/facebook-icon.svg';

export default class Index extends Component
{
    constructor()
    {
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
            validation: true,
            errorStatus: false,
            hidden: true,
            message: '',
            email: '',
            password: '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
    }

    //Action button submit
    handleSubmit(e)
    {
        e.preventDefault();


        if(this.state.email.length && this.state.password.length)
        {
            //Validation Form correct.
            this.setState({validation: true});

            //Get and save email and password
            const access = {
                username: this.state.email,
                password: this.state.password,
            }

            axios.post(`${process.env.API_URL}oauth/token` ,access)
                .then(response => {

                    //If it's not success.
                    if(("exito" in  response.data) && !(response.data.exito))
                    {
                        this.setState({validation: false, message: "Error. Check your credentials"});
                    }else
                    {
                        this.setState({validation: true});
                        localStorage.setItem('token',response.data.access_token);

                        window.location = `${process.env.BASE_URL}/dashboard`;
                    }
                }).catch((error) => {
                    this.setState({errorStatus:true, message: "Error Network"});
                });
        }
        else
        {
            this.setState({validation: false, message: "Error. Check your credentials"});
        }

    }


    // Catch email.
    handleEmail(e)
    {
        this.setState({email:e.target.value});
    }

    //Catch password
    handlePassword(e)
    {
        this.setState({password:e.target.value});
    }

    responseFacebook = (response) => {

        if(response.accessToken == null)
        {
            this.setState({errorStatus:true, message: "Facebook app hasn't been accepted"});
        }
        else {
            const access = {
                fb_access_token : response.accessToken
            }

            axios.post(`${process.env.API_URL}auth/fb` ,access)
                .then( response_tergar => {
                    localStorage.setItem('token', response_tergar.data.access_token);
                    window.location = `${process.env.BASE_URL}/dashboard`;
                }).catch(() => {
                this.setState({errorStatus:true, message: "Error Network"});
            });
        }
    }

    componentDidMount()
    {
        //If I have a new message from another side.
        if("message" in  localStorage)
        {
            this.setState({validation: true, message: localStorage.getItem("message"), hidden: false});
            localStorage.removeItem("message");
        }
    }

    render() {
        return (
            <LoginForm>
                <div>
                    <form method="POST" >
                        <Logotipo src={logotipo_tergar} alt="Tergar" />
                        <FormValidation message={this.state.translation[this.state.message]} validation={this.state.validation} status={this.state.errorStatus} hidden={this.state.hidden} />
                        <UserInput type="email" name="username" placeholder={this.state.translation['Email']} onChange={this.handleEmail} />
                        <PasswordInput type="password" name="password" placeholder={this.state.translation['Password']} onChange={this.handlePassword} />
                        {/*<p className="text-right pt6"><Link to="/recover_password">{this.state.translation['Forgot password?']}</Link></p>*/}

                        <TergarButton type="submit" onClick={this.handleSubmit}>{this.state.translation['Log in']}</TergarButton>
                        {/*<FacebookLogin
                            appId="1192373547477228"
                            textButton = {this.state.translation['Login with Facebook']}
                            fields="name,email,picture"
                            callback={this.responseFacebook}
                            cssClass="iniciar-facebook"
                            icon={<img
                                src="/img/facebook-icon.svg" />}
                            version="2.8"
                        />*/}
                        <FacebookLogin
                            appId="1192373547477228"
                            textButton = {this.state.translation['Login with Facebook']}
                            fields="name,email,picture"
                            callback={this.responseFacebook}
                            version="2.8"
                            render = { renderProps =>
                                (<FBButton onClick={renderProps.onClick}>
                                    <FBimg src={facebook_icon}/>
                                    {this.state.translation['Login with Facebook']}
                                </FBButton>)}
                        />

                        <Register>
                            <Link to="/register">{this.state.translation['Create Tergar ID']}</Link>
                        </Register>
                    </form>
                </div>
                <GlobalStyle/>
            </LoginForm>
        );
    }
}