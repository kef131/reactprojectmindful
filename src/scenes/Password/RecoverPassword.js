import React, { Component } from 'react';
import {Link} from "react-router-dom";
import GlobalStyle from '../Styles/Login/GlobalStyle';
import Language from '../../components/Language';
import validator from 'validator';
import FormError from "./components/FormValidation";
import axios from 'axios';
import {LoginForm, EmailInput, Register, Logotipo} from '../Styles/Login/General';
import {TergarButton} from "../Styles/Login/Buttons";
import logotipo_tergar from '../../img/logotipo-tergar.svg';

export default class RecoverPassword extends Component
{
    constructor()
    {
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
            email:'',
            validation: true,
            errorStatus: false,
            message: '',
            hidden: true,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
    }

    // Catch Email.
    handleEmail(e)
    {
        this.setState({email:e.target.value});
    }

    handleSubmit(e)
    {
        e.preventDefault();


            if(validator.isEmail(this.state.email))
            {
                this.setState({validation: true});

                let access = {
                    email: this.state.email,
                }

                axios.post(`${process.env.API_URL}user/forgotPassword`,access)
                    .then(response => {

                        //If it's not success.
                        if(response.data.exito)
                        {
                            //window.location = "/register";
                            localStorage.setItem("email",this.state.email);
                            this.setState({errorStatus: false , message: response.data.mensaje, hidden: false});
                        }else
                        {
                            localStorage.setItem("email",this.state.email);
                            this.setState({errorStatus:true, message: response.data.mensaje});

                        }

                    }).catch((error) => {
                    this.setState({errorStatus:true, message: "Error Network"});
                });
            }
            else
            {
                this.setState({validation: false, message: "Wrong email user. Try again."});
            }

    }
    render() {
        return (
            <div className="container h100p">
                <div className="row h100p">
                    <LoginForm>
                        <div>
                            <form method="post">
                                <Logotipo src={logotipo_tergar} alt="Tergar" />
                                <FormError message={this.state.message} validation={this.state.validation} status={this.state.errorStatus} hidden={this.state.hidden}/>
                                    <p>{this.state.translation['Enter your email to recover your password:']}</p>
                                    <EmailInput type="email" name="email" id="email" placeholder={this.state.translation['Email']} onChange={this.handleEmail} />
                                    <TergarButton type="submit" onClick={this.handleSubmit}>{this.state.translation['Send email']}</TergarButton>
                                        <p><small>{this.state.translation['You will receive an email with the recover code that you need to copy and paste in the next screen']}</small></p>
                                        <Register><Link to="/login" >{this.state.translation['Back to Login']}</Link></Register>
                            </form>
                        </div>
                    </LoginForm>
                </div>
                <GlobalStyle/>
            </div>
        );
    }
}