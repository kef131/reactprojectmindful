import React , { Component } from 'react';
import {Link} from 'react-router-dom';
import Language from '../../components/Language';
import validator from 'validator';
import axios from 'axios';
import FormValidation from "./components/FormValidation";
import GlobalStyle from "../Styles/Login/GlobalStyle";
import {LoginForm, NewPasswordInput, Register, Logotipo} from '../Styles/Login/General';
import {TergarButton} from '../Styles/Login/Buttons';
import logotipo_tergar from '../../img/logotipo-tergar.svg';

export default class ResetPassword extends Component
{
    constructor(props)
    {
        super(props);

        if(props.match.params.token.length !== 10)
        {
            window.location = `${process.env.BASE_URL}/login`;
        }
        else
        {
            this.state = {
                translation: Language[localStorage.getItem("language")],
                token: props.match.params.token,
                validation: true,
                errorStatus: false,
                hidden: true,
                message: '',
                password: '',
                newPassword: '',
            };

            this.handlePassword = this.handlePassword.bind(this);
            this.handleRepeatPassword = this.handleRepeatPassword.bind(this);
            this.handleSubmit = this.handleSubmit.bind(this);
        }
    }

    // Catch Password.
    handlePassword(e)
    {
        this.setState({password:e.target.value});
    }

    // Catch New Password.
    handleRepeatPassword(e)
    {
        this.setState({newPassword:e.target.value});
    }

    //
    handleSubmit(e)
    {
        e.preventDefault();


        if(!validator.isEmpty(this.state.password) && !validator.isEmpty(this.state.newPassword))
        {
            if(validator.equals(this.state.password,this.state.newPassword))
            {
                if((this.state.password.length < 8) && (this.state.newPassword.length < 8))
                {
                    this.setState({validation: false, message: "Your password must contain at least 8 characters"});
                }
                else
                {
                    //Validation Form correct.
                    this.setState({validation: true});

                    const access = {
                        email: localStorage.getItem("email"),
                        token: this.state.token,
                        newPassword: this.state.newPassword,
                    }

                    axios.post(`${process.env.API_URL}user/resetPassword`,access)
                        .then(response => {

                            //If it is successful.
                            if(response.data.exito)
                            {

                                this.setState({errorStatus: false , hidden: true});
                                localStorage.removeItem("email");
                                localStorage.setItem("message","Your password has been reseted. Please login.");
                                //Redirect to login
                                window.location = `${process.env.BASE_URL}/login`;
                            }else
                            {
                                this.setState({errorStatus:true, message: "Password not updated. Recovery code or User not found."});

                            }
                        }).catch((error) => {
                        this.setState({errorStatus:true, message: "Error Network"});
                    });
                }

            }
            else
            {
                this.setState({validation: false, message: "The password does not match"});
            }
        }
        else
        {
            this.setState({validation: false, message: "All the fields are important"});
        }
    }
    render() {
        return (
            <div className="container h100p">
                <div className="row h100p">
                    <LoginForm>
                        <div>
                            <form method="post">
                                <Logotipo src={logotipo_tergar} alt="Tergar" />
                                <FormValidation message={this.state.translation[this.state.message]} validation={this.state.validation} status={this.state.errorStatus} hidden={this.state.hidden}/>
                                    <p>{this.state.translation['Enter the recovery code that was sent to your email and set your new password:']}</p>
                                    <NewPasswordInput type="password" placeholder={this.state.translation['New password']} name="password" id="password" onChange={this.handlePassword}/>
                                    <NewPasswordInput type="password" placeholder={this.state.translation['Repeat Password']} name="password_confirmation" id="password_confirmation" onChange={this.handleRepeatPassword}/>
                                    <TergarButton type="submit" onClick={this.handleSubmit}>{this.state.translation['Submit']}</TergarButton>
                                    <Register><Link to="/login">{this.state.translation['Back to Login']}</Link></Register>
                            </form>
                        </div>
                    </LoginForm>
                </div>
                <GlobalStyle/>
            </div>
        );
    }
}