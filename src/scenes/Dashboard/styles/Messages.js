import styled from "styled-components";

export const CajaMensaje = styled.div`
  background-color: rgba(255,255,255,1);
  box-shadow: 0 0 14px rgba(0,0,0,0.17);
  box-sizing: border-box;
  padding:16px;
  width: 340px;
  border-radius:4px;
`;

export const TituloMensaje = styled.h2`
  height: 32px;
  color: #5c6bc0;
  margin: 0 0 6px;
  display: block;
  font-weight: 300;
`;

export const TextTitulo = styled.span`
  font-size: 18px;
  display: inline-block;
  vertical-align: middle;
`;

export const IconotTitulo = styled.span`
  display: inline-block;
  vertical-align: middle;
  height: 32px;
  margin-right: 10px;
`;

export const TextMensaje = styled.p`
  font-size: 12px;
  display: block;
  padding: 1px 5px;
  margin: 0 ;
  line
`;
