import styled from "styled-components";
import { createGlobalStyle } from "styled-components";

import Bkmenu01 from '../../../img/icon/men-01.svg';
import Bkmenu02 from '../../../img/icon/men-02.svg';
import Bkmenu03 from '../../../img/icon/men-03.svg';
import Bkmenu04 from '../../../img/icon/men-04.svg';
import Bkmenu05 from '../../../img/icon/men-05.svg';
import Bkmenu06 from '../../../img/icon/men-06.svg';
import Bkmenu07 from '../../../img/icon/men-07.svg';
import Bkmenu08 from '../../../img/icon/men-08.svg';
import Bkmenu09 from '../../../img/icon/men-09.svg';
import Bkmenu10 from '../../../img/icon/men-10.svg';
import Bkmenu11 from '../../../img/icon/men-11.svg';
import Bkmenu12 from '../../../img/icon/men-12.svg';
import Bkmenu13 from '../../../img/icon/men-13.svg';
import BkTwitter from '../../../img/icon/twitter.svg';
import BkInstagram from '../../../img/icon/instagram.svg';
import BkFacebook from '../../../img/icon/facebook.svg';
import Bkmenu01a from '../../../img/icon/men-01a.svg';
import Bkmenu02a from '../../../img/icon/men-02a.svg';
import Bkmenu03a from '../../../img/icon/men-03a.svg';
import Bkmenu04a from '../../../img/icon/men-04a.svg';
import Bkmenu05a from '../../../img/icon/men-05a.svg';
import Bkmenu06a from '../../../img/icon/men-06a.svg';
import Bkmenu07a from '../../../img/icon/men-07a.svg';
import Bkmenu08a from '../../../img/icon/men-08a.svg';
import Bkmenu09a from '../../../img/icon/men-09a.svg';
import Bkmenu10a from '../../../img/icon/men-10a.svg';
import Bkmenu11a from '../../../img/icon/men-11a.svg';
import Bkmenu12a from '../../../img/icon/men-12a.svg';
import Bkmenu13a from '../../../img/icon/men-13a.svg';
import Bkmenu13aa from '../../../img/icon/men-13aa.svg';

const estiloglobal = createGlobalStyle`
@import url('https://fonts.googleapis.com/css?family=Roboto:400,500');
html,body{font-family: 'Roboto', sans-serif; font-weight: 400; color:#727070; font-size: 12px; margin:0; line-height: 1.52857143; height: 100%; }
body{background: url(http://brain-think.com/tergar-app/img/bk/bk-body.svg) no-repeat bottom; background-size: 100%; background-attachment: fixed; }
`;

export const  HeadMovil = styled.div`
  background-color: rgba(255,255,255,.94);
  padding: 8px 18px;
  box-shadow: 0 0 16px rgba(0,0,0,.25);
  width: 100%; 
  position: fixed;
  top: 0;
  left:0;
  z-index:2;
`;

export const WrapperApp = styled.div`
  padding: 16px 30px 0 60px;
  width: 100%; 

  @media  (max-width: 991px){  
  }
  
  @media  (max-width: 767px){
    padding: 80px 10px 0;
  }
`;

export const WraperMenuWebaap = styled.div`
height: 100%; 
width: 42px; 
position: fixed; 
z-index: 3; 
top: 0; 
left: 10px; 
transition: 0.5s; 
padding: 16px 0;
`;

export const LogotipoMenu = styled.div`
  overflow: hidden;
  margin-bottom: 32px;
`;

const LinkMenu = styled.a`
  background-position: center;
  background-repeat: no-repeat;
  background-size: 100%;
  height: 18px; 
  width: 18px; 
  display: block;
  margin: 0 auto 16px;
  cursor: pointer;
  text-decoration: none !important;
`;

export const Menu01 = styled(LinkMenu)`
  background-image: url(${Bkmenu01});
  
  &:hover{
  background-image: url(${Bkmenu01a}); 
`;

export const Menu02 = styled(LinkMenu)`
  background-image: url(${Bkmenu02});

  &:hover{
  background-image: url(${Bkmenu02a});
`;

export const Menu03 = styled(LinkMenu)`
  width: 20px; 
  height: 20px;
  background-image: url(${Bkmenu03});

  &:hover{
  background-image: url(${Bkmenu03a});  
`;

export const Menu04 = styled(LinkMenu)`
  background-image: url(${Bkmenu04});

  &:hover{
  background-image: url(${Bkmenu04a});  
`;

export const Menu05 = styled(LinkMenu)`
  background-image: url(${Bkmenu05});

  &:hover{
  background-image: url(${Bkmenu05a});  
`;

export const Menu06 = styled(LinkMenu)`
  background-image: url(${Bkmenu06});

  &:hover{
  background-image: url(${Bkmenu06a});  
`;

export const Menu07 = styled(LinkMenu)` 
  height: 20px;
  background-image: url(${Bkmenu07});

  &:hover{
  background-image: url(${Bkmenu07a});  
`;

export const Menu08 = styled(LinkMenu)`
  width: 19px; 
  background-image: url(${Bkmenu08});

  &:hover{
  background-image: url(${Bkmenu08a});  
`;

export const Menu09 = styled(LinkMenu)`
  width: 21px; 
  height: 20px;
  background-image: url(${Bkmenu09});

  &:hover{
  background-image: url(${Bkmenu09a});  
`;

export const MenuIdioma = styled(LinkMenu)`
  background-image: url(${Bkmenu12});
  font-size: 8px;
  color: #5c6bc0;
  text-align: center;
  padding-top: 1px;
  font-weight: 500;
  margin-top: 32px;

  &:hover{
  background-image: url(${Bkmenu12a});
  color: #ff9333; 
`;

export const MenuAyuda = styled(LinkMenu)`
  background-image: url(${Bkmenu10});

  &:hover{
  background-image: url(${Bkmenu10a}); 
`;

export const MenuConfiguracion = styled(LinkMenu)`
  background-image: url(${Bkmenu11});

  &:hover{
  background-image: url(${Bkmenu11a}); 
`;

export const MenuSalir = styled(LinkMenu)`
  background-image: url(${Bkmenu13});

  &:hover{
  background-image: url(${Bkmenu13a}); 
  
`;

export const MenuSalirMobile = styled.button`
  background-position: center;
  background-repeat: no-repeat;
  background-image: url(${Bkmenu13});
  background-color:transparent;
  background-size: 70%;
  display:block;
  margin: 4px 0 0;
  float: right;
  border: 1px solid #5c6bc0;
  border-radius: 4px;
  height: 24px; 
  width: 24px; 

  &:hover{
  background-image: url(${Bkmenu13aa});
  background-color: #5c6bc0;
  } 
`;

export const WraperRedesIndicacdores = styled.div`
  height: 100%; 
  width: 40px; 
  position: fixed; 
  z-index: 3; 
  top: 0; 
  right: 0px; 
  transition: 0.5s; 
  padding: 16px 0;
  text-aling:center;
`;

const LinkRedSocial = styled.a`
  transition: 0.5s; 
  background-position: center;
  background-repeat: no-repeat;
  background-size: 100%;
  height: 16px; 
  width: 16px;
  margin: 0 auto 16px;
  display:block;
  cursor: pointer;
  
  &:hover{
    opacity: .7;
  }
`;

export const Twitter = styled(LinkRedSocial)`
  background-image: url(${BkTwitter});  
`;

export const Instagram = styled(LinkRedSocial)`
  background-image: url(${BkInstagram});  
`;

export const Facebook = styled(LinkRedSocial)`
  background-image: url(${BkFacebook});  
`;

export const ContenedorMenuBottom = styled.div`
  position:absolute;
  bottom:0;
  width: 100%;
  padding-bottom:16px;
  
  @media  (max-height: 520px){
    position: relative;
  }
`;

export const Indicador = styled.a`
  background-color: #d8d8d8;
  height: 6px; 
  width: 6px;
  display:block;
  margin: 0 auto 16px;
  border-radius: 50px
  cursor:pointer;

  &:hover{
    background-color: #ff9333;
  }
`;

export const TituloSeccion = styled.h2`
  color: #5c6bc0;
  font-size: 26px;
  font-weight: 500;
  margin-top: 0px;
  margin-bottom: 5px;
  text-transform: uppercase;
`;

export const FraseSeccion = styled.h3`
  font-size: 16px;
  font-weight: 400;
  font-style: italic;
  margin: 0;
`;

export const Txtligh = styled.span`
  font-weight: 400;
`;

export const LineaTitulo = styled.hr`
  background: #5c6bc0;
  width: 100px;
  height: 2px;
  margin: 10px 0 32px;
`;

export const ClearFix = styled.div`
  clear: both;
`;

export const NegroBack = styled.section`
  padding: 8em;
  background: #000000;
  
  @media  (max-width: 767px){
    padding: 8em .8em;
  }
`;

export const CajaBlanca = styled.div`
  background-color: rgba(255, 255, 255, 0.9);
  box-shadow: 0 0 14px rgba(0, 0, 0, 0.17);
  box-sizing: border-box;
  padding: 16px;
  border-radius: 4px;
`;