import styled from 'styled-components';

export const SectionTitle = styled.h2`
  color: #5c6bc0;
  font-size: 26px;
  font-weight: 400;
  margin-top: 20px;
  margin-bottom: 5px;
  text-transform: uppercase;
`;

export const Phrase = styled.h3`
  font-size: 16px;
  font-weight: 300;
  font-style: italic;
  margin: 0;
`;

export const TitleUnderline = styled.hr`
  background: #5c6bc0;
  width: 100px;
  height: 2px;
  margin: 10px 0 32px;
`;

export const Txtligh = styled.span`
  font-weight: 300;
`;
