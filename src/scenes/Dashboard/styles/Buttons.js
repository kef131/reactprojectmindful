import styled from "styled-components";
import discard from '../../../img/icon/discard.svg';
import finish from '../../../img/icon/finish.svg';
import playbco from '../../../img/icon/playbco.svg';
import pausabco from '../../../img/icon/pausabco.svg';

const Btn = styled.button`
  background-color:transparent;
  border-radius: 4px;
  border-style: solid;
  border-width: 1px;
  border-color: #5C6BC0;
  color: #5C6BC0;
  text-align: center;
  transition: all 0.3s ease;
  margin: 10px 4px 2px;
  width:70px;
  padding: 3px;
`;

export const BtnNormal = styled(Btn)`
  &:hover{
    background-color: #5C6BC0;
    color: #fff;
    opacity: 0.8;
`;

export const BtnResaltado = styled(Btn)`
  background-color: #5C6BC0;
  color: #fff;

  &:hover{
    background-color: #5C6BC0;
    color: #fff;
    opacity: 0.8;
`;

//BtnResaltadoBco
export const BtnWhiteHighlighted = styled(Btn)`
  border-color: #fff;
  color: #fff;
  width: 100px;
  margin-top:20px;

  &:hover{
    opacity: 0.98;
    background-color: #fff;
    color:#000;
`;


export const ClearFix = styled.div`
  clear: both;
`;

export const BtnDiscart = styled.button`
  background-color: transparent;
  color: #fff;
  width: 40px;
  height: 40px;
  background-position: center;
	background-repeat: no-repeat;
  background-image:url(${discard});
  border: 1px solid #fff; 
  border-radius: 100px;
  background-size: 16px;
  transition: all 0.3s ease;

  &:hover{
    background-color: rgba(255,255,255,.2);
`;

export const BtnFinishSession = styled.button`
  background-color: transparent;
  color: #fff;
  width: 40px;
  height: 40px;
  background-position: center;
	background-repeat: no-repeat;
  background-image:url(${finish});
  border: 1px solid #fff; 
  border-radius: 100px;
  background-size: 16px;
  transition: all 0.3s ease;

  &:hover{
    background-color: rgba(255,255,255,.2);
`;

export const BtnContinueSession = styled.button`
  background-color: transparent;
  color: #fff;
  width: 70px;
  height: 70px;
  background-position: center;
	background-repeat: no-repeat;
  background-image:url(${playbco});
  border: 1px solid #fff; 
  border-radius: 100px;
  background-size: 24px;
  transition: all 0.3s ease;
  margin: 0 30px;

  &:hover{
    background-color: rgba(255,255,255,.2);
`;

export const BtnPauseSession = styled.button`
  background-color: transparent;
  color: #fff;
  width: 70px;
  height: 70px;
  background-position: center;
	background-repeat: no-repeat;
  background-image:url(${pausabco});
  border: 1px solid #fff; 
  border-radius: 100px;
  background-size: 24px;
  transition: all 0.3s ease;
  margin: 0 30px;

  &:hover{
    background-color: rgba(255,255,255,.2);
`;

//Button used for calendars

//New button calendar white
export const BtnBcoCalendar = styled(Btn)`
  border-color: #fff;
  color: #fff;
  width: 100px;
  margin:0;

  &:hover{
    opacity: 0.98;
    background-color: #fff;
    color:#ff9333;
  }
  
  @media  (max-width: 767px){
    width: 22px;
    height: 22px;
    padding: 0;
    text-align: center;
    font-size: 20px;
    line-height: 0;
  }
`;