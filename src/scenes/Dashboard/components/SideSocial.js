import React, {Component} from 'react';
import twitter from '../../../img/icon/twitter.svg';
import instagram from '../../../img/icon/instagram.svg';
import facebook from '../../../img/icon/facebook.svg';

export default class SideSocial extends Component
{
    render() {
        return (
            <div className="side-redes">
                <a href="#" target="_blank"><img src={twitter} className="img-responsive"
                                                 alt="Twitter Tergar" /></a>
                <a href="#" target="_blank"><img src={instagram} className="img-responsive"
                                                 alt="Instagram Tergar" /></a>
                <a href="#" target="_blank"><img src={facebook} className="img-responsive"
                                                 alt="Facebook Tergar" /></a>
                <div className="indicadores">
                    <a href="#"></a>
                    <a href="#"></a>
                    {/*<a href="#" className="indicador-activo"></a>
                    <a href="#"></a>
                    <a href="#"></a>
                    <a href="#"></a>
                    <a href="#"></a>
                    <a href="#"></a>
                    <a href="#"></a>*/}
                </div>
            </div>
        );
    }
}