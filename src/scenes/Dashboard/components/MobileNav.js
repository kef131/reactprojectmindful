import React, {Component} from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import Language from "../../../components/Language";
import logotipo_tergar2 from '../../../img/icon/logotipo-tergar2.svg';

export default class MobileNav extends Component
{
    constructor()
    {
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
        }
    }
    logout = () =>
    {
        localStorage.clear();
        window.location = `${process.env.BASE_URL}/login`;
    }

    render() {
        return (
            <nav className="navbar navbar-inverse visible-xs visible-sm">
                <div className="navbar-header">
                    <a className="navbar-brand" href="#"><img src={logotipo_tergar2} height="36" alt="Tergar" /></a>
                    <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                </div>

                <div className="navbar-collapse collapse">
                    <ul className="nav navbar-nav">
                        {/*<li className="active"><a href="#" className="menu01">Profile</a></li>*/}
                        <li><AnchorLink href="#timer" className="menu02">{this.state.translation["Timer"]}</AnchorLink></li>
                        {/*<li><a href="#" className="menu03">Counter</a></li>*/}
                        <li><AnchorLink href="#calendar" className="menu04">{this.state.translation["Calendar"]}</AnchorLink></li>
                        <li><a className="menu12" onClick={this.logout}>{this.state.translation["Logout"]}</a></li>
                        {/*<li><a href="#" className="menu05">Statistics</a></li>
                        <li><a href="#" className="menu06">Goals</a></li>
                        <li><a href="#" className="menu07">Courses</a></li>
                        <li><a href="#" className="menu08">Learning</a></li>
                        <li><a href="#" className="menu09">Community</a></li>*/}
                    </ul>
                </div>
            </nav>
    );
    }
}