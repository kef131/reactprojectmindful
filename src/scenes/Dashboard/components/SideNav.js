import React, { Component } from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import logotipo_tergar from '../../../img/icon/logotipo-tergar.svg';

export default class SideNav extends Component
{
    constructor() {
        super();

        this.state = {
            active: 2,
            className: "menu-seleccionado seleccionado",
        }

    }

    onClickActive = (e) =>
    {
        this.setState({active: e.currentTarget.dataset.id});
    }
    logout = () =>
    {
        localStorage.clear();
        window.location = `${process.env.BASE_URL}/login`;
    }

    render() {
        return (
            <div className="sidenav">
                <img src={logotipo_tergar} className="logo-tergar mb32" alt="Tergar" />
                    <div className="cont-menu">
                        {/*<a href="#" className="menu01"></a>*/}
                        <AnchorLink href="#timer" data-id={2} className={(this.state.active == 2) ? "menu02 " + this.state.className + "02" : "menu02"} onClick={this.onClickActive} ></AnchorLink>
                        {/*<a href="#" className="menu03"></a>*/}
                        <AnchorLink href="#calendar" data-id={4} className={(this.state.active == 4) ? "menu04 " + this.state.className + "04" : "menu04"} onClick={this.onClickActive}></AnchorLink>
                        <a className="menu12" onClick={this.logout}></a>
                        {/*<a href="#" className="menu05"></a>
                        <a href="#" className="menu06"></a>
                        <a href="#" className="menu07"></a>
                        <a href="#" className="menu08"></a>
                        <a href="#" className="menu09"></a>*/}
                    </div>
                    {/*<div className="opcion-extra">
                        <a href="#" className="btn-idioma">EN</a>
                        <a href="#" className="menu10"></a>
                        <a href="#" className="menu11"></a>
                    </div>*/}
            </div>

        );
    }
}