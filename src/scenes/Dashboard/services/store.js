import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import {Redirect} from 'react-router-dom';
import React from "react";

/*const logger = store => next => action => {
    console.log('dispatching', action)
    let result = next(action)
    console.log('next state', store.getState())
    return result
}*/

const dataMeditation = (state = [], action) => {

    //console.log(state);
    switch (action.type) {
        case "LOAD_MEDITATION_SESSIONS":
            return action.data;

        case "CREATE_MEDITATION_SESSION":
            return [...state, action.data.valor];

        case "UPDATE_MEDITATION_SESSION":
            return [...state.filter(data => data.id !== action.data.valor.id ),action.data.valor];

        case "REMOVE_MEDITATION_SESSION":
            console.log(...state.filter(data => data.id !== action.id ));

            return [...state.filter(data => data.id !== action.id )];
        default:
            return state;
    }
}

const dataMalaMeditation = (state = [], action) =>
{
    //console.log(state);
    switch (action.type) {
        case "LOAD_MALA_SESSIONS":
            return action.data;

        case "CREATE_MALA_SESSION":
            return [...state,action.data.valor];

        case "UPDATE_MALA_SESSION":
            return [...state.filter(data => data.id !== action.data.valor.id ),action.data.valor];

        case "REMOVE_MALA_SESSION":
            //window.location.href = "/dashboard";
            return [...state.filter(data => data.id !== action.id )];
        default:
            return state;
    }
}

const personalInformation = (state = [], action) =>
{
    switch (action.type)
    {
        case "LOAD_INFORMATION":
            localStorage.setItem("user",JSON.stringify(action.data));
            return action.data;
        default:
            return state;
    }
}

const courses = (state = [], action) =>
{
    switch (action.type)
    {
        case "FILTER_COURSES":
            console.log(state);
            return [...state.filter(data => data.course.code === action.data )];

        default:
            return state;
    }
}

export default createStore(combineReducers({dataMeditation, dataMalaMeditation,personalInformation, courses}), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(thunk));