import axios from 'axios';

const loadPersonalInformation = () =>
{
    return async dispatch => {
        return axios.get(`${process.env.API_URL}me`,{
            headers: {Authorization: "Bearer " + localStorage.getItem("token")}
        }).then(response => {
            dispatch({
                type: "LOAD_INFORMATION",
                data: response.data.valor,
            })
        });
    }
}
const loadMeditationSession = () =>
{
    return dispatch => {
        return axios.get(`${process.env.API_URL}meditation`,{
            headers: {Authorization: "Bearer " + localStorage.getItem("token")}
        }).then(response => {
            dispatch({
                type: "LOAD_MEDITATION_SESSIONS",
                data: response.data.valor,
            })
        });
    }
}

const loadMalaMeditationSession = () =>
{
    return dispatch => {
        return axios.get(`${process.env.API_URL}mala/meditation`,{
            headers: {Authorization: "Bearer " + localStorage.getItem("token")}
        }).then(response => {
            dispatch({
                type: "LOAD_MALA_SESSIONS",
                data: response.data.valor,
            })
        });
    }
}

const updateSession = (info,ismala) =>
{
    if(ismala)
    {
        return dispatch => {
            return axios.post(`${process.env.API_URL}mala/meditation`,info)
                .then(response => {
                    dispatch({
                        type: "UPDATE_MALA_SESSION",
                        data: response.data,
                    })
                });
        }
    }
    else
    {
        return dispatch => {
            return axios.post(`${process.env.API_URL}meditation`, info)
                .then(response => {
                    dispatch({
                        type: "UPDATE_MEDITATION_SESSION",
                        data: response.data,
                    })
                });
        }
    }
}

const removeSession = (id,ismala) => {

    if(ismala)
    {
        return dispatch => {
            return axios.delete(`${process.env.API_URL}mala/meditation` + "/" + id)
                .then(response => {
                    dispatch({
                        type: "REMOVE_MALA_SESSION",
                        data: response.data,
                        id: id,
                    })
                });
        }
    }
    else
    {
        return dispatch => {
            return axios.delete(`${process.env.API_URL}meditation` + "/" + id)
                .then(response => {
                    dispatch({
                        type: "REMOVE_MEDITATION_SESSION",
                        data: response.data,
                        id: id,
                    })
                });
        }
    }
}

const createSession = (session,ismala) =>
{

    if(ismala)
    {
        return dispatch => {
            return axios.post(`${process.env.API_URL}mala/meditation`,session)
                .then(response => {

                    localStorage.setItem("message_status", response.data.exito);
                    dispatch({
                        type: "CREATE_MALA_SESSION",
                        data: response.data,
                    })
            });
        }
    }
    else
    {
        return dispatch => {
            return axios.post(`${process.env.API_URL}meditation`,session)
                .then(response => {
                    localStorage.setItem("message_status", response.data.exito);
                    dispatch({
                        type: "CREATE_MEDITATION_SESSION",
                        data: response.data,
                    })
            });
        }
    }
}

const filterPractices = (code) =>
{
    /*let id;
    if(!("user" in localStorage))
    {
        loadPersonalInformation();
    }

    id = JSON.parse(localStorage.getItem("user")).id;*/


    return dispatch => {
        dispatch({
            type: "FILTER_COURSES",
            data: code,
        });
    }
}
export {loadMeditationSession, loadMalaMeditationSession, updateSession, removeSession, createSession, loadPersonalInformation, filterPractices};