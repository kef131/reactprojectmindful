import styled from "styled-components";

import arrow_left from '../../../../../img/icon/arrow-l.svg';
import arrow_right from '../../../../../img/icon/arrow-r.svg';
import ico_duracion from '../../../../../img/icon/ico-duracion.svg';
import ico_cuentas from '../../../../../img/icon/ico-cuentas.svg';

export const ContenedorSombraRedondeado = styled.div`
	box-shadow: 0 0 14px rgba(0,0,0,.17);
	border-radius: 4px;
	overflow: hidden;
	background: rgba(255,255,255,.90);
	height: 514px;
	margin-bottom:80px;
	
	@media  (max-width: 767px){
	   height: auto;
	}
`;

export const EncabezadoNaranja = styled.div`
	background: #ff9333;
	padding: 15px 6px;
	height: 56px;
	color:#fff;

	@media  (max-width: 1024px){
		padding: 15px 5px;
	}

	@media  (max-width: 767px){
		padding: 12px 5px;
		height: 50px;
	}
`;

const FlechaAnio = styled.button`
	width: 10px;
	display: inline-block;
	vertical-align: middle;
	height: 15px;
	background-repeat: no-repeat;
	background-position: center center;
	background-size: contain;
	background-color: transparent;
	border: none;

	&:hover{
		opacity: .75;
	}
`;

export const FlechaIzquierda = styled(FlechaAnio)`
	background-image: url(${arrow_left});
`;

export const FlechaDerecha = styled(FlechaAnio)`
	background-image: url(${arrow_right});
`;

export const Anio = styled.span`
	display: inline-block;
	vertical-align: middle;
	font-size: 20px;
	margin: 3px 4px;
	line-height: 20px;

	@media  (max-width: 767px){
		display:none;
	}
`;

export const AnioMes = styled.span`
	display:none;
	font-size: 15px;
	line-height: 14px;
	padding: 1px 5px 0;
	vertical-align: middle;

	@media  (max-width: 767px){
		display:inline-block;
	}
`;

export const CuerpoCalendario = styled.div`
	padding: 20px;

	@media  (max-width: 1024px){
		padding: 32px 20px;
		height: 454px;
	}

	@media  (max-width: 991px){
		padding: 0 20px 32px; height: 360px;
	}

	@media  (max-width: 767px){
		padding: 0 12px 12px;
		height: auto;
	}
`;

export const ListaMeses = styled.ul`
	padding: 0;
	border-bottom: 1px solid #b8b8b8;
	list-style: none;
	margin: 0;
	
	@media  (max-width: 991px){
		padding: 16px 0 0;
	}
`;

export const ElementoMes = styled.li`
	float:left;
	width: 8.33%;
	text-align: center;
`;

export const BotonMes = styled.button`
	color: #727070;
	border-top: none;
	border-left: none;
	border-right: none;
	border-bottom: 3px solid #fff;
	transition: all 0.3s ease;
	background: none;
	padding: 0;

	&:hover{
		border-bottom: 3px solid #ff9333 !important;
		color:#ff9333 !important;
		display: inline-block;
		padding: 0 0 6px;
		text-decoration: none;
	}
`;

export const BotonMesActivo = styled.button`
	background: none;
	border-top: none;
	border-left: none;
	border-right: none;

	border-bottom: 3px solid #ff9333 !important;
	color:#ff9333 !important;
	display: inline-block;
	padding: 0 0 6px;
	text-decoration: none;
`;

export const TablaCalendario = styled.table`
	margin-top: 16px;
	width: 100%;
	border-collapse: collapse;
`;

export const RenglonCalendario = styled.tr`
	
`;

export const EncabezadoCalendario = styled.th`
	text-align: center;
	font-size: 14px;
	color:#5C6BC0;
	font-weight: 400;
	padding: 16px 0;
	
	@media  (max-width: 767px){
		padding: 8px 0;
	}
`;

export const EncabezadoCalendarioMovil = styled.span`
	display:none;

	@media  (max-width: 991px){
		display:block;
	}

	@media  (max-width: 767px){
		display:block;
	}
`;

export const EncabezadoCalendarioPC = styled.span`
	@media  (max-width: 991px){
		display:none;
	}

	@media  (max-width: 767px){
		display:none;
	}
`;

const CeldaDia = styled.td`
	text-align: center;
	padding: 16px 0;
	font-size: 14px;
	font-weight: 300;
	position:relative;
	z-index:1;
`;

export const DiaDesactivado = styled(CeldaDia)`
	color:#B8B8B8;
	
`;

export const DiaNormal = styled(CeldaDia)`
   cursor: pointer; 
`;

export const DiaNoSeleccionable = styled(CeldaDia)`
    
`;

export const DiaSesion = styled(CeldaDia)`
  cursor: pointer;
  
  &:before {
      content: " ";
      position: absolute;
      background: rgba(92,107,192,0.10);
      left: 50%;
      top:50%;
      border-radius: 50px;
      width: 40px;
      height: 40px;
      margin-top: -20px;
      margin-left: -20px;
   }
   
    &:hover {
      &:before {
        background-color: rgba(92,107,192,0.25);        
        opacity: 0.8;
      }
    }
    
    @media  (max-width: 767px){
      &:before {
        width: 32px;
        height: 32px;
        margin-top: -17px;
        margin-left: -16px;
      }
    }
`;

export const DiaSeleccionado = styled(CeldaDia)`
   color:#fff;
   cursor: pointer;
   
   &:before {
      content: " ";
      position: absolute;
      background: rgba(92,107,192,0.99);
      left: 50%;
      top:50%;
      border-radius: 50px;
      width: 40px;
      height: 40px;
      margin-top: -20px;
      margin-left: -20px;
      z-index: -1;
   }
   
    @media  (max-width: 767px){
      &:before {
        width: 32px;
        height: 32px;
        margin-top: -17px;
        margin-left: -16px;
      }
    }

`;

export const FechaEncabezado = styled.p`
	line-height: 14px;
    width: 100px;
    float: right;
    margin: -2px 0 0;
`;

export const DiaEncabezado = styled.span`
	font-size:20px;
`;

export const CuerpoListadoDetalle = styled.div`
	padding: 0 24px;
	height: calc(100% - 56px);
	overflow-y: scroll;

	@media  (max-width: 1024px){
		padding: 0 20px;
	}

	@media  (max-width: 991px){
		
	}
`;


export const ElementoListadoPractica = styled.div`
	border-bottom: 1px solid #b8b8b8;
	padding-bottom: 16px;
	margin-top: 24px;
`;

export const TituloListadoPractica = styled.h4`
	overflow: hidden;
	margin-bottom: 2px;
	height: 18px;
	font-size: 14px;
	
`;

const ElementoListadoPracticaDetalle = styled.p`
	color:#B8B8B8;
	padding-left: 18px;
	background-position: left center;
	background-repeat: no-repeat;
	background-size: 16px;
	display: inline-block;
	vertical-align: middle;
	margin-bottom: 2px;
	font-weight: 400;
`;

export const ElementoListadoPracticaDuracion = styled(ElementoListadoPracticaDetalle)`
	background-image: url(${ico_duracion});
	margin-right: 30px;
`;

export const ElementoListadoPracticaMalas = styled(ElementoListadoPracticaDetalle)`
	background-image: url(${ico_cuentas});
`;

export const ElementoListadoPracticaImg = styled.img`
	width: 18px;
	height: 18px;
	margin: 2px;
`;

const EtiquetaGenerica = styled.label`
	display: block;
	color:#2F3D6E;
	font-size: 12px;
	font-style: italic;
	font-weight: 400;
	margin: 0;
`;

export const EtiquetaMargen = styled(EtiquetaGenerica)`
	margin-top: 32px;
`;

export const Etiqueta = styled(EtiquetaGenerica)`
	
`;

export const CampoMitadIzquierda = styled.label`
	float: left;
	width: 46.5%;
`;

export const CampoMitadDerecha = styled.label`
	width: 46.5%;
	float: right;
`;



export const ImgPerfil = styled.img`
	width: 799px;
	margin-bottom:80px;
	
	@media  (max-width: 991px){
		width: 100%;
	}
`;