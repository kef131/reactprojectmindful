import React, { Component } from 'react';
import dateFns from 'date-fns';
import MessageDelete from './MessageDelete';
import ListCourse from "./ListCourse";
import connect from "react-redux/es/connect/connect";
import {updateSession} from "../../../services/actionCreators";
import MessageSaved from "./MessageSaved";
import arrow_back from '../../../../../img/icon/arrow-back.svg';
import btn_save from '../../../../../img/icon/btn-save.svg';
import btn_delete from '../../../../../img/icon/btn-delete.svg';
import TimePicker from 'rc-time-picker';
import moment from 'moment';
import {
    CampoMitadDerecha,
    CampoMitadIzquierda,
    ContenedorSombraRedondeado,
    CuerpoListadoDetalle,
    EncabezadoNaranja, Etiqueta,
    EtiquetaMargen
} from "../styles/CalendarStyle";
import {BtnBcoCalendar} from "../../../styles/Buttons";

class EditMeditation extends  Component
{
    constructor()
    {
        super();

        this.state = {
            validation: true,
            errorStatus: false,
            isHiddenMessageDelete: true,
            isHiddenMessageSaved: true,
            message: '',
            isClickFeeling: false,
            isClickPlace: false,
            isChangePractice: false,
            isChangeBaseBeads: false,
            isChangeMalas: false,
            isChangeBeads: false,
            isChangeElapsed: false,
            practiceName: '',
            place: 0,
            idPractice: 0,
            elapsed: 0,
            baseBeads: 7,
            beads: 7,
            malaSize: 1,
            feeling: 1,
            elapsedObject: moment().utcOffset(0).set({hour:0,minute:1,second:0,millisecond:0}),
        };

        this.saveInformation = this.saveInformation.bind(this);
        this.handleFeeling = this.handleFeeling.bind(this);
        this.getPractice = this.getPractice.bind(this);
        this.deleteSession = this.deleteSession.bind(this);

        this.changeBaseBeads = this.changeBaseBeads.bind(this);
        this.changeMalas = this.changeMalas.bind(this);
        this.changeBeads = this.changeBeads.bind(this);

        this.handleTime = this.handleTime.bind(this);

        this.returnBackPressButton = this.returnBackPressButton.bind(this);
        //Create inputs ref.

        this.baseBeadsRef = React.createRef();
        this.malaSizeRef = React.createRef();
        this.beadsRef = React.createRef();
    }

    //Convert HH:mm:ss to seconds.
    convertHmsToSeconds = (time) =>
    {
        let [h,m,s] = time.split(':');

        return (parseInt(h)) * 3600 + (parseInt(m)) * 60 + (parseInt(s));
    }

    secondsToHms = (t) => {

        let h = Math.floor(t / 3600);
        let m = Math.floor(t % 3600 / 60);
        let s = Math.floor(t % 60);

        let hDisplay = h < 10 ? "0" + h.toString() : h.toString();
        let mDisplay = m < 10 ? "0" + m.toString() : m.toString();
        let sDisplay = s < 10 ? "0" + s.toString() : s.toString();

        return hDisplay +":"+ mDisplay+":"+sDisplay;
    }

    initializeElapsedMomentObject = (t) => {

        let h = Math.floor(t / 3600);
        let m = Math.floor(t % 3600 / 60);
        let s = Math.floor(t % 60);

        let momentObject = moment().utcOffset(0).set({hour:h,minute:m,second:s,millisecond:0});
        
        return momentObject;
    }

    saveInformation = (e) => {

        e.preventDefault();

        let elapsedObject = this.state.elapsedObject;

        //Validation
        let isMala = e.currentTarget.dataset.ismala;
        let malaSize = (this.state.isChangeBaseBeads) ? this.state.baseBeads : parseInt(e.currentTarget.dataset.malasize);
        let beads = (this.state.isChangeBeads) ? this.state.beads : parseInt(e.currentTarget.dataset.beads);
        let timeElapsed = (this.state.isChangeElapsed) ? this.convertHmsToSeconds(elapsedObject.format("HH:mm:ss")) : parseInt(e.currentTarget.dataset.time);
        let feeling = (this.state.isClickFeeling) ? this.changeFeeling(this.state.feeling) : this.changeFeeling(parseInt(e.currentTarget.dataset.feeling));
        let place = (this.state.isClickPlace) ? this.changePlace(this.state.place) : this.changePlace(parseInt(e.currentTarget.dataset.place));
        let idPractice =  (this.state.isChangePractice) ?  this.state.idPractice : parseInt(e.currentTarget.dataset.idpractice)
        let date = parseInt(e.currentTarget.dataset.date);

        if(isMala == "true")
        {
            const MalaMeditation = {
                access_token: localStorage.getItem("token"),
                beads,
                malaSize,
                feeling,
                place,
                course: idPractice,
                id: parseInt(e.currentTarget.dataset.idsession),
                elapsed: timeElapsed,
                date,
            }

            this.props.updateSession(MalaMeditation,true);
        }
        else
        {
            const Meditation = {
                access_token: localStorage.getItem("token"),
                feeling,
                place,
                course_id: idPractice,
                id:  parseInt(e.currentTarget.dataset.idsession),
                elapsed: timeElapsed,
                date,
            }

            this.props.updateSession(Meditation,false);

        }

        this.setState({isHiddenMessageSaved: false});
    }

    deleteSession= () =>
    {
        //this.props.isHiddenEdit(true);
        this.setState({isHiddenMessageDelete: false});
    }

    changeFeeling = (feeling) =>
    {
        switch (feeling) {
            case 0:
                return "DISTRACTED";
            case 1:
                return "HAPPY";
            case 2:
                return "BORED";
            case 3:
                return "NORMAL";
        }
    }

    changePlace = (place) =>
    {
        switch (place) {
            case 0:
                return "WORK";
            case 1:
                return "HOME";
            case 2:
                return "TERGAR";
            case 3:
                return "OUTSIDE";
        }
    }

    changeBaseBeads = (e) =>
    {
        const baseBeads = parseInt(e.currentTarget.value);
        const malaSize = parseInt(this.malaSizeRef.current.value);
        this.setState({baseBeads, beads: baseBeads * malaSize, isChangeBaseBeads : true, isChangeBeads: true});
    }


    changeMalas = (e) =>
    {
        const baseBeads = parseInt(this.baseBeadsRef.current.value);
        const malaSize = parseInt(e.currentTarget.value);

        this.setState({malaSize, beads: baseBeads * malaSize, isChangeMalas : true, isChangeBeads: true});
    }

    changeBeads = (e) =>
    {
        let baseBeads = parseInt(this.baseBeadsRef.current.value);
        let beads = parseInt(e.currentTarget.value);
        let malaSize = Math.round(beads/baseBeads);

        beads = baseBeads * malaSize;

        this.setState({malaSize, beads, isChangeMalas : true, isChangeBeads: true});
    }

    handleFeeling = (e) => {
        this.setState({feeling: parseInt(e.currentTarget.dataset.id), isClickFeeling: true});
    }

    handleLocation = (e) => {
        this.setState({place: parseInt(e.currentTarget.dataset.id), isClickPlace: true});
    }

    //Get time of form.
    handleTime(time) {
        time = (time != null) ? time : moment().utcOffset(0).set({hour:0,minute:1,second:0,millisecond:0});
        this.setState({elapsedObject:time, isChangeElapsed: true});
    }

    getPractice = (course) =>
    {
        this.setState({practiceName: course.name, idPractice: parseInt(course.id), isChangePractice: true});
    }

    changeHiddenMessageDelete = (hidden) =>
    {
        this.setState({isHiddenMessageDelete : hidden});
    }

    changeHiddenEdit = (hidden) =>
    {
        this.props.isHiddenEdit(hidden);
    }

    returnBackPressButton = (e) =>
    {
        this.props.onBackPressButton(e.currentTarget.dataset.hidden);
    }

    switchFeeling = (selectedFeeling) => {
        switch (selectedFeeling) {
        case "DISTRACTED":
        case 0:
            return (
                <div className="scroll-animo-lugares">
                    <span><a className="edo-animo01" data-id="1" onClick={this.handleFeeling}>Happy</a></span>
                    <span><a className="edo-animo02-activo" data-id="0" onClick={this.handleFeeling}>Distracted</a></span>
                    <span><a className="edo-animo03" data-id="2" onClick={this.handleFeeling}>Bored</a></span>
                    <span><a className="edo-animo04" data-id="3" onClick={this.handleFeeling}>Normal</a></span>
                </div>);
        case "HAPPY":
        case 1:
            return (<div className="scroll-animo-lugares">
                <span><a className="edo-animo01-activo" data-id="1" onClick={this.handleFeeling}>Happy</a></span>
                <span><a className="edo-animo02" data-id="0" onClick={this.handleFeeling}>Distracted</a></span>
                <span><a className="edo-animo03" data-id="2" onClick={this.handleFeeling}>Bored</a></span>
                <span><a className="edo-animo04" data-id="3" onClick={this.handleFeeling}>Normal</a></span>
            </div>);
        case "BORED":
        case 2:
            return (<div className="scroll-animo-lugares">
                <span><a className="edo-animo01" data-id="1" onClick={this.handleFeeling}>Happy</a></span>
                <span><a className="edo-animo02" data-id="0" onClick={this.handleFeeling}>Distracted</a></span>
                <span><a className="edo-animo03-activo" data-id="2" onClick={this.handleFeeling}>Bored</a></span>
                <span><a className="edo-animo04" data-id="3" onClick={this.handleFeeling}>Normal</a></span>
            </div>);
        case "NORMAL":
        case 3:
            return (<div className="scroll-animo-lugares">
                <span><a className="edo-animo01" data-id="1" onClick={this.handleFeeling}>Happy</a></span>
                <span><a className="edo-animo02" data-id="0" onClick={this.handleFeeling}>Distracted</a></span>
                <span><a className="edo-animo03" data-id="2" onClick={this.handleFeeling}>Bored</a></span>
                <span><a className="edo-animo04-activo" data-id="3" onClick={this.handleFeeling}>Normal</a></span>
            </div>);
        }
    }

    switchPlace = (selectedPlace) =>
    {
        switch (selectedPlace) {
            case "WORK":
            case 0:
                return (<div className="scroll-animo-lugares">
                    <span><a className="lugar-pactica01-activo" data-id="0" onClick={this.handleLocation}>Work</a></span>
                    <span><a className="lugar-pactica02" data-id="1" onClick={this.handleLocation}>Home</a></span>
                    <span><a className="lugar-pactica03" data-id="2" onClick={this.handleLocation}>Tergar</a></span>
                    <span><a className="lugar-pactica04" data-id="3" onClick={this.handleLocation}>Outside</a></span>
                </div>);
                break;
            case "HOME":
            case 1:
                return (<div className="scroll-animo-lugares">
                    <span><a className="lugar-pactica01" data-id="0" onClick={this.handleLocation}>Work</a></span>
                    <span><a className="lugar-pactica02-activo" data-id="1" onClick={this.handleLocation}>Home</a></span>
                    <span><a className="lugar-pactica03" data-id="2" onClick={this.handleLocation}>Tergar</a></span>
                    <span><a className="lugar-pactica04" data-id="3" onClick={this.handleLocation}>Outside</a></span>
                </div>);
            case "TERGAR":
            case 2:
                return (<div className="scroll-animo-lugares">
                    <span><a className="lugar-pactica01" data-id="0" onClick={this.handleLocation}>Work</a></span>
                    <span><a className="lugar-pactica02" data-id="1" onClick={this.handleLocation}>Home</a></span>
                    <span><a className="lugar-pactica03-activo" data-id="2" onClick={this.handleLocation}>Tergar</a></span>
                    <span><a className="lugar-pactica04" data-id="3" onClick={this.handleLocation}>Outside</a></span>
                </div>);
            case "OUTSIDE":
            case 3:
                return (<div className="scroll-animo-lugares">
                    <span><a className="lugar-pactica01" data-id="0" onClick={this.handleLocation}>Work</a></span>
                    <span><a className="lugar-pactica02" data-id="1" onClick={this.handleLocation}>Home</a></span>
                    <span><a className="lugar-pactica03" data-id="2" onClick={this.handleLocation}>Tergar</a></span>
                    <span><a className="lugar-pactica04-activo" data-id="3" onClick={this.handleLocation}>Outside</a></span>
                </div>);
        }
    }

    render() {

        //Get session information.
        let data = (this.props.ismala) ? this.props.dataMalaMeditation.filter(data => data.id == this.props.idPractice) : this.props.dataMeditation.filter(data => data.id == this.props.idPractice);

        let typeList = (this.props.ismala) ? "Mala" : "Meditation";

        //Get feeling icon by feeling value.
        let selectedFeeling = (this.state.isClickFeeling) ? this.switchFeeling(this.state.feeling) : this.switchFeeling(data[0].feeling);
        //Get place icon by place value.
        let selectedPlace = (this.state.isClickPlace) ? this.switchPlace(this.state.place) : this.switchPlace(data[0].place);
        //Get course name.
        let selectedPractice = (this.state.isChangePractice) ? this.state.practiceName : data[0].course.name;
        //Get base beads (beads per mala).
        let newBaseBeads = (this.state.isChangeBaseBeads) ? this.state.baseBeads : (data[0].malaSize !== undefined) ?  parseInt(data[0].malaSize) : this.state.baseBeads;
        //Get malas.
        let newMalas = (this.state.isChangeMalas) ? this.state.malaSize : (data[0].malaSize !== undefined || data[0].beads !== undefined) ? parseInt(data[0].beads / data[0].malaSize) : this.state.malaSize;
        //Get total Beads.
        let newBeads = (this.state.isChangeBeads) ? this.state.beads : (data[0].beads !== undefined) ? parseInt(data[0].beads) : this.state.beads;
        //Hide/Show mala section.
        let display = this.props.ismala ? 'block' : 'none';

        let elapsedValue = (this.state.isChangeElapsed) ? this.state.elapsedObject : this.initializeElapsedMomentObject( data[0].elapsed );

        return (
            <div className="col-xs-12 col-sm-5 col-md-4 mt16">
                <ContenedorSombraRedondeado>
                    <EncabezadoNaranja>
                        <div className="col-xs-12">
                            <BtnBcoCalendar data-hidden={true} onClick={this.returnBackPressButton}>
                                Back
                            </BtnBcoCalendar>
                        </div>
                    </EncabezadoNaranja>


                    <CuerpoListadoDetalle>


                            <EtiquetaMargen>Course</EtiquetaMargen>
                            <ListCourse codePractice= {data[0].course.code} selectedPractice={selectedPractice} newPractice={this.getPractice} typeList = {typeList} />

                            <div className="clearfix"></div>

                            <CampoMitadIzquierda >
                                <Etiqueta>Date</Etiqueta>
                                <div className="input-group date form_datetime">
                                    <input className="form-control" size="16" type="text" defaultValue={dateFns.format(data[0].date,'YYYY-MM-DD')}
                                           placeholder="DD-MM-AA" disabled/>
                                    <span className="input-group-addon"><span
                                        className="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </CampoMitadIzquierda>

                            <CampoMitadDerecha>
                                <Etiqueta>Time</Etiqueta>
                                <div className="input-group">
                                    <TimePicker className="form-control" onChange={this.handleTime} showSecond={false} value={elapsedValue}/>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-time"></i></span>
                                </div>
                            </CampoMitadDerecha>

                            <div className="clearfix"></div>

                            {/* Beads fault part*/}
                            <div style={{display: display}}>
                                <div><div className="campo30 f-left  mb16">
                                    <label>Beads per mala<span className="ocultar"></span></label>
                                    <input className="form-control  text-center"  type="number" min="7" max="216" placeholder="7" value={newBaseBeads} onChange={this.changeBaseBeads} ref={this.baseBeadsRef}/>
                                </div>

                                    <div className="f-left signo  mb16">X</div>

                                    <div className="campo30 f-left mb16">
                                        <label>Malas</label>
                                        <input className="form-control  text-center" type="number" min="1" value={newMalas} onChange={this.changeMalas} ref={this.malaSizeRef} />
                                    </div>

                                    <div className="f-left signo font16 mb16">=</div>

                                    <div className="campo30 f-left mb16">
                                        <label>Beads</label>
                                        <input className="form-control text-center" type="number" value={newBeads} onChange={this.changeBeads} ref={this.beadsRef} />
                                    </div>
                                </div>
                            </div>


                        <div className="clearfix"></div>

                        <div className="estado-animo-lugares mb16">
                            <label>Feelings</label>
                            {selectedFeeling}
                        </div>

                        <div className="clearfix"></div>

                        <div className="estado-animo-lugares mb32">
                            <label>Place</label>
                            {selectedPlace}
                        </div>

                        <div className="clearfix"></div>

                        {/*btn save*/}
                        <div className="campo50 mb32 f-left">
                            <button className="guardar" onClick={this.saveInformation} data-feeling={data[0].feeling} data-place={data[0].place} data-ismala={this.props.ismala} data-idsession={data[0].id} data-idpractice = {data[0].course.id} data-malasize = {data[0].malaSize} data-beads = {data[0].beads} data-time = {data[0].elapsed} data-date = {data[0].date}>
                                <img src={btn_save} alt="Add Session" />
                                <hr /><span>Save</span>
                            </button>
                        </div>

                        {/*delete button*/}
                        <div className="campo50 mb32 f-right">
                            <button className="eliminar" onClick={this.deleteSession} data-delete={false} >
                                <img src={btn_delete} alt="Delete Session" />
                                <hr /><span>Delete</span>
                            </button>
                        </div>

                    </CuerpoListadoDetalle>
                </ContenedorSombraRedondeado>
                <MessageDelete hidden={this.state.isHiddenMessageDelete} ismala = {data[0].course.is_mala_course} id={data[0].id} changeHiddenMessageDelete = {this.changeHiddenMessageDelete} changeHiddenEdit = {this.changeHiddenEdit} />
                {!this.state.isHiddenMessageSaved && <MessageSaved />}
            </div>
        );
    }
}

const mapStateToProps =  state =>
{
    return{
        dataMeditation : state.dataMeditation,
        dataMalaMeditation: state.dataMalaMeditation,
    }
}

const mapDispatchToProps = dispatch =>
{
    return {
        updateSession (info,ismala)
        {
            dispatch(updateSession(info,ismala));
        }
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(EditMeditation);