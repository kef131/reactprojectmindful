import React, {Component} from 'react';

export default class ListCourse extends Component
{

    constructor()
    {
        super();
        this.handleNewPractice = this.handleNewPractice.bind(this);
    }

    handleNewPractice = (e) =>
    {
        this.props.newPractice({id: e.currentTarget.dataset.id, ismala: e.currentTarget.dataset.ismala, name:  e.currentTarget.dataset.name });
    }

    listDropdownAll = () =>
    {
        return (
            <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li data-name="Joy of Living 1" data-code="JOL1" data-id="1" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Joy of Living 1</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Joy of Living 2" data-code="JOL2" data-id="2" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Joy of Living 2</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Joy of Living 3" data-code="JOL3" data-id="4" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Joy of Living 3</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Guru Yoga" data-code="GURU_YOGA" data-id="3" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">Guru Yoga</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Nature of mind" data-code="NOM" data-id="5" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Nature of mind</a></li>
            <li role="separator" className="divider"></li>
            <li  data-name="Vajrasattva" data-code="VAJRASATTVA" data-id="7" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">Vajrasattva</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Prostrations" data-code="PROSTRATIONS" data-id="8" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">Prostrations</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Mandala" data-code="MANDALA" data-id="9" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">Mandala</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Tara" data-code="TARA" data-id="10" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">Tara</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Nectar of the Path" data-code="NECTAR_PATH" data-id="11" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Nectar of the Path</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="White Tara Long" data-code="WHITE_TARA_LONG" data-id="13" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">White Tara Long</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Mindfulness" data-code="MINDFULNESS" data-id="14" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Mindfulness</a></li>
            </ul>
            )
    }

    listDropdownMala = () =>
    {
        return (<ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li data-name="Guru Yoga" data-code="GURU_YOGA" data-id="3" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">Guru Yoga</a></li>
            <li role="separator" className="divider"></li>
            <li  data-name="Vajrasattva" data-code="VAJRASATTVA" data-id="7" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">Vajrasattva</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Prostrations" data-code="PROSTRATIONS" data-id="8" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">Prostrations</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Mandala" data-code="MANDALA" data-id="9" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">Mandala</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Tara" data-code="TARA" data-id="10" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">Tara</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="White Tara Long" data-code="WHITE_TARA_LONG" data-id="13" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">White Tara Long</a></li>
        </ul>)
    }

    listDropdownMeditation = () =>
    {
        return (<ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li data-name="Joy of Living 1" data-code="JOL1" data-id="1" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Joy of Living 1</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Joy of Living 2" data-code="JOL2" data-id="2" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Joy of Living 2</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Joy of Living 3" data-code="JOL3" data-id="4" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Joy of Living 3</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Nature of mind" data-code="NOM" data-id="5" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Nature of mind</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Nectar of the Path" data-code="NECTAR_PATH" data-id="11" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Nectar of the Path</a></li>
            <li role="separator" className="divider"></li>
            <li data-name="Mindfulness" data-code="MINDFULNESS" data-id="14" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">Mindfulness</a></li>
        </ul>)
    }
    render() {

        let list = null;
        switch (this.props.typeList) {
            case "Mala":
                list = this.listDropdownMala();
                break;
            case "Meditation" :
                list = this.listDropdownMeditation();
                break;
            default:
                list = this.listDropdownAll();
                break;
        }
        return (
            <div className="dropdown mb16">
                <button className="btn btn-default dropdown-toggle" type="button" id="dropdownMenu3"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    {this.props.selectedPractice}
                    <span className="caret"></span>
                </button>
                {list}
            </div>
        );
    }
}