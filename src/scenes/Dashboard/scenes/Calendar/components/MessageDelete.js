import React, { Component } from 'react';
import {connect} from 'react-redux';
import {removeSession} from '../../../services/actionCreators';
import MessageSaved from "./MessageSaved";
import {TituloMensaje, CajaMensaje, IconotTitulo, TextMensaje, TextTitulo} from '../../../styles/Messages';
import { BtnNormal, BtnResaltado, ClearFix } from "../../../styles/Buttons";
import icon_delete from '../../../../../img/icon/delete.svg';

class MessageDelete extends Component{

    constructor ()
    {
        super();

        this.state = {
            isHiddenMessageSaved: true,
        }
    }

    changeHidden = () =>
    {
        //let element = document.getElementsByClassName("modal-delete");
        //element[0].parentNode.removeChild(element[0]);
        this.props.changeHiddenMessageDelete(true);

    }

    removeSession = () => {
        this.setState({isHiddenMessageSaved: false});
        this.props.removeSession(this.props.id,this.props.ismala);
        this.props.changeHiddenEdit(true);
    }
    render() {

        if(!this.props.hidden) {
            return (
                <div className="modal-delete">
                <div className="modal-color">
                    <CajaMensaje>
                        <TituloMensaje>
                            <IconotTitulo>
                                <img
                                    src={icon_delete}
                                    height="100%"
                                />
                            </IconotTitulo>
                            <TextTitulo>Delete this session</TextTitulo>
                        </TituloMensaje>
                        <TextMensaje>
                            This action cannot be undone. Are you sure you want to delete it?
                        </TextMensaje>
                        <BtnNormal onClick={this.removeSession}>Delete</BtnNormal>
                        <BtnResaltado onClick={this.changeHidden}>Cancel</BtnResaltado>
                        <ClearFix />
                    </CajaMensaje>
                    {!this.state.isHiddenMessageSaved && <MessageSaved />}
                </div>
                </div>
            );
        }
        else
        {
            return null;
        }
    }
}


const mapDispatchToProps = dispatch =>
{
    return {
        removeSession (id,ismala)
        {
            dispatch(removeSession(id,ismala));
        }
    };
}

export default connect(0,mapDispatchToProps)(MessageDelete);