import React, {Component} from 'react';
import {connect} from 'react-redux';
import {filterPractices} from "../../../services/actionCreators";
import Language from "../../../../../components/Language";

class FilterByCourse extends Component
{

    constructor()
    {
        super();
        this.state = {
            translation: Language[localStorage.getItem("language")],
        }
        this.handleNewPractice = this.handleNewPractice.bind(this);
    }

    handleNewPractice = (e) =>
    {
        this.props.getPractice(e.currentTarget.dataset);

        let {code,id,ismala} = e.currentTarget.dataset;

        this.props.filterPractices(code)
    }


    render() {

        return (
            <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li data-name="All courses" data-code="All"  onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['All courses']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="Joy of Living 1" data-code="JOL1" data-id="1" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Joy of Living 1']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="Joy of Living 2" data-code="JOL2" data-id="2" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Joy of Living 2']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="Joy of Living 3" data-code="JOL3" data-id="4" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Joy of Living 3']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="Guru Yoga" data-code="GURU_YOGA" data-id="3" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Guru Yoga']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="Nature of mind" data-code="NOM" data-id="5" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Nature of mind']}</a></li>
                <li role="separator" className="divider"></li>
                <li  data-name="Vajrasattva" data-code="VAJRASATTVA" data-id="7" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Vajrasattva']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="Prostrations" data-code="PROSTRATIONS" data-id="8" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Prostrations']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="Mandala" data-code="MANDALA" data-id="9" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Mandala']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="Tara" data-code="TARA" data-id="10" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Tara']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="Nectar of the Path" data-code="NECTAR_PATH" data-id="11" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Nectar of the Path']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="White Tara Long" data-code="WHITE_TARA_LONG" data-id="13" data-ismala="1" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['White Tara Long']}</a></li>
                <li role="separator" className="divider"></li>
                <li data-name="Mindfulness" data-code="MINDFULNESS" data-id="14" data-ismala="0" onClick={this.handleNewPractice}><a href="javascript:void(0)">{this.state.translation['Mindfulness']}</a></li>
            </ul>
        );
    }
}

const mapStateToProps =  state =>
{
    return{

    }
}

const mapDispatchToProps = dispatch =>
{
    return {
        filterPractices (code)
        {
            dispatch(filterPractices(code));
        }
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(FilterByCourse);