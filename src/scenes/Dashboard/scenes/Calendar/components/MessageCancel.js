import React, {Component} from 'react';

import {TituloMensaje, CajaMensaje, IconotTitulo, TextMensaje, TextTitulo} from '../../../styles/Messages';
import { BtnNormal, BtnResaltado, ClearFix } from "../../../styles/Buttons";
import warning from '../../../../../img/icon/warning.svg';

export default class MessageCancel extends Component{

    changeCancel = () =>
    {
        this.props.changeHiddenMessageCancel(true);
    }

    changeClose = () =>
    {
        this.props.changeToClose(true);
    }

    saveSession = () =>
    {
        this.props.changeHidden(true);
    }

    render() {
        return (
            <div className="modal-delete">
                <div className="modal-color">
                    <CajaMensaje>
                        <TituloMensaje>
                            <IconotTitulo>
                                <img
                                    src={warning}
                                    height="100%"
                                />
                            </IconotTitulo>
                            <TextTitulo>Close without saving</TextTitulo>
                        </TituloMensaje>
                        <TextMensaje>
                            You have not saved this changes. Are you sure you want to close?
                        </TextMensaje>
                        <BtnNormal onClick={this.changeClose}>Close</BtnNormal>
                        <BtnResaltado onClick={this.changeCancel}>Cancel</BtnResaltado>
                        <ClearFix />
                    </CajaMensaje>
                </div>
            </div>
        );
    }
}