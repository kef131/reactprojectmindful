import React, {Component} from 'react';
import {TituloMensaje, CajaMensaje, IconotTitulo, TextMensaje, TextTitulo} from '../../../styles/Messages';
import Language from "../../../../../components/Language";
import good from '../../../../../img/icon/good.svg';

export default class MessageSaved extends Component{

    constructor ()
    {
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
            redirect: false,
        }
    }

    render() {
        setInterval(() => this.setState({redirect: true}), 3000);
        if (this.state.redirect) window.location.href = `${process.env.BASE_URL}/dashboard`;

        return (
            <div className="modal-delete">
                <div className="modal-color">
                    <CajaMensaje>
                        <TituloMensaje>
                            <IconotTitulo>
                                <img
                                    src={good}
                                    height="100%"
                                />
                            </IconotTitulo>
                            <TextTitulo>{this.state.translation["Awesome"]}</TextTitulo>
                        </TituloMensaje>
                        <TextMensaje>
                            {this.state.translation["Session has been saved!"]} <br />
                            {this.state.translation["Check it in your practice diary."]}
                        </TextMensaje>
                    </CajaMensaje>
                </div>
            </div>
        );
    }
}