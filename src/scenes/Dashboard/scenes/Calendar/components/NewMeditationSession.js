import React, {Component} from 'react';
import MessageCancel from './MessageCancel';
import ListCourse from "./ListCourse";
import {createSession} from '../../../services/actionCreators';
import {connect} from 'react-redux';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import TimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';
import 'react-datepicker/dist/react-datepicker.css';
import MessageSaved from "./MessageSaved";
import Language from "../../../../../components/Language";
import btn_save from '../../../../../img/icon/btn-save.svg';
import btn_cancel from '../../../../../img/icon/btn-cancel.svg';

class NewMeditationSession extends Component {

    constructor(){
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
            selectedPractice: 'Joy of Living 1',
            isMalaSession: 0,
            isHiddenMessageCancel: true,
            isHiddenMessageSaved: true,
            isHiddenSavedMessage: true,
            baseBeads: 7,
            isClickFeeling: false,
            isClickPlace: false,
            idPractice : 1,
            place: 1,
            elapsed: moment().utcOffset(0).set({hour:0,minute:1,second:0,millisecond:0}),
            date: moment(),
            feeling: 1,
            malaSize: 1,
            beads: 7,
        }

        this.beadsInput = React.createRef();
        this.changeBaseBeads = this.changeBaseBeads.bind(this);
        this.changeMalas = this.changeMalas.bind(this);
        this.handleDate = this.handleDate.bind(this);
        this.handleTime = this.handleTime.bind(this);
    }
    changeHiddenMessageCancel = (hidden) =>
    {
        this.setState({isHiddenMessageCancel: hidden});
    }

    changeToClose = () =>
    {
        this.props.changeHidden(true);
        //this.props.changeHidden(hidden);
    }

    cancelSession = () =>
    {
        this.setState({isHiddenMessageCancel: false});
    }

    getPractice = (practice) =>
    {
        this.setState({selectedPractice: practice.name, isMalaSession: parseInt(practice.ismala), idPractice : parseInt(practice.id)});
    }

    changeBaseBeads = (e) =>
    {
        this.calculateMalasBeads(e.currentTarget.value,this.state.malaSize);
        this.setState({baseBeads: e.currentTarget.value});
    }

    changeMalas = (e) =>
    {
        this.calculateMalasBeads(this.state.baseBeads,e.currentTarget.value);
            this.setState({malaSize: e.currentTarget.value});
    }

    calculateMalasBeads = (baseBeads,malaSize) =>
    {

        this.beadsInput.current.value = parseInt(baseBeads) * parseInt(malaSize);
        this.setState({beads: baseBeads * malaSize});
    }

    handleFeeling = (e) => {
        this.setState({feeling: parseInt(e.currentTarget.dataset.id), isClickFeeling: true});
    }

    handleLocation = (e) => {
        this.setState({place: parseInt(e.currentTarget.dataset.id), isClickPlace: true});
    }

    //Save my new session.
    saveSession = () =>
    {
        //Get if it is a session with or without malas.

        let newSession = [];

        if(this.state.isMalaSession)
        {
            let {feeling,place,idPractice, malaSize, beads, elapsed, date} = this.state;

            newSession = {
                access_token: localStorage.getItem('token'),
                beads: beads,
                malaSize: malaSize,
                feeling: this.switchFeeling(feeling),
                place: this.switchPlace(place),
                course: idPractice,
                elapsed: this.convertHmsToSeconds(elapsed.format("HH:mm:ss")),
                date: date.format('YYYY-MM-DD HH:mm:ss'),
            }

            this.props.createSession(newSession,this.state.isMalaSession);
        }
        else
        {
            let {feeling,place,idPractice, elapsed, date} = this.state;

            newSession = {
                access_token: localStorage.getItem('token'),
                feeling: this.switchFeeling(feeling),
                place: this.switchPlace(place),
                course_id: idPractice,
                elapsed: this.convertHmsToSeconds(elapsed.format("HH:mm:ss")),
                date: date.format('YYYY-MM-DD HH:mm:ss'),
            }


            this.props.createSession(newSession,this.state.isMalaSession);
        }

        this.setState({isHiddenMessageSaved: false});
    }

    switchFeeling = (feeling) =>
    {
        switch (feeling) {
            case 0:
                return "DISTRACTED";
            case 1:
                return "HAPPY";
            case 2:
                return "BORED";
            case 3:
                return "NORMAL";
        }
    }

    switchPlace = (place) =>
    {
        switch (place) {
            case 0:
                return "WORK";
            case 1:
                return "HOME";
            case 2:
                return "TERGAR";
            case 3:
                return "OUTSIDE";
        }
    }

    //Get date of form.
    handleDate(date)
    {
        this.setState({date});
    }

    handleChangeRaw = (date) => { date.currentTarget.value = moment(this.state.date).format("DD/MM/YYYY")};

    //Get time of form.
    handleTime(time) {
        time = (time != null) ? time : moment().utcOffset(0).set({hour:0,minute:1,second:0,millisecond:0});
        this.setState({elapsed:time});
    }

    //Convert HH:mm:ss to seconds.
    convertHmsToSeconds = (time) =>
    {
        let [h,m,s] = time.split(':');

        return (parseInt(h)) * 3600 + (parseInt(m)) * 60 + (parseInt(s));
    }
    render() {

        let display = (this.state.isMalaSession)? 'block' : 'none';
        let selectedFeeling = this.state.feeling;
        let selectedPlace = this.state.place;
        let listFeeling = '', listLocation = '';

        switch (selectedFeeling) {
            case 0:
                listFeeling = <div className="scroll-animo-lugares">
                    <span><a className="edo-animo02-activo" data-id="0" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Distracted']}</a></span>
                    <span><a className="edo-animo01" data-id="1" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Happy']}</a></span>
                    <span><a className="edo-animo03" data-id="2" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Bored']}</a></span>
                    <span><a className="edo-animo04" data-id="3" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Normal']}</a></span>
                </div>;
                break;
            case 1:
                listFeeling = <div className="scroll-animo-lugares">
                    <span><a className="edo-animo02" data-id="0" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Distracted']}</a></span>
                    <span><a className="edo-animo01-activo" data-id="1" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Happy']}</a></span>
                    <span><a className="edo-animo03" data-id="2" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Bored']}</a></span>
                    <span><a className="edo-animo04" data-id="3" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Normal']}</a></span>
                </div>
                break;
            case 2:
                listFeeling = <div className="scroll-animo-lugares">
                    <span><a className="edo-animo02" data-id="0" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Distracted']}</a></span>
                    <span><a className="edo-animo01" data-id="1" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Happy']}</a></span>
                    <span><a className="edo-animo03-activo" data-id="2" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Bored']}</a></span>
                    <span><a className="edo-animo04" data-id="3" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Normal']}</a></span>
                </div>
                break;
            case 3:
                listFeeling = <div className="scroll-animo-lugares">
                    <span><a className="edo-animo02" data-id="0" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Distracted']}</a></span>
                    <span><a className="edo-animo01" data-id="1" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Happy']}</a></span>
                    <span><a className="edo-animo03" data-id="2" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Bored']}</a></span>
                    <span><a className="edo-animo04-activo" data-id="3" onClick={this.handleFeeling}>{this.state.translation['Feeling']['Normal']}</a></span>
                </div>
                break;
        }

        switch (selectedPlace) {
            case 0:
                listLocation = <div className="scroll-animo-lugares">
                    <span><a className="lugar-pactica01-activo" data-id="0" onClick={this.handleLocation}>{this.state.translation['Place']['Work']}</a></span>
                    <span><a className="lugar-pactica02" data-id="1" onClick={this.handleLocation}>{this.state.translation['Place']['Home']}</a></span>
                    <span><a className="lugar-pactica03" data-id="2" onClick={this.handleLocation}>{this.state.translation['Place']['Tergar']}</a></span>
                    <span><a className="lugar-pactica04" data-id="3" onClick={this.handleLocation}>{this.state.translation['Place']['Outside']}</a></span>
                </div>;
                break;
            case 1:
                listLocation = <div className="scroll-animo-lugares">
                    <span><a className="lugar-pactica01" data-id="0" onClick={this.handleLocation}>{this.state.translation['Place']['Work']}</a></span>
                    <span><a className="lugar-pactica02-activo" data-id="1" onClick={this.handleLocation}>{this.state.translation['Place']['Home']}</a></span>
                    <span><a className="lugar-pactica03" data-id="2" onClick={this.handleLocation}>{this.state.translation['Place']['Tergar']}</a></span>
                    <span><a className="lugar-pactica04" data-id="3" onClick={this.handleLocation}>{this.state.translation['Place']['Outside']}</a></span>
                </div>
                break;
            case 2:
                listLocation = <div className="scroll-animo-lugares">
                    <span><a className="lugar-pactica01" data-id="0" onClick={this.handleLocation}>{this.state.translation['Place']['Work']}</a></span>
                    <span><a className="lugar-pactica02" data-id="1" onClick={this.handleLocation}>{this.state.translation['Place']['Home']}</a></span>
                    <span><a className="lugar-pactica03-activo" data-id="2" onClick={this.handleLocation}>{this.state.translation['Place']['Tergar']}</a></span>
                    <span><a className="lugar-pactica04" data-id="3" onClick={this.handleLocation}>{this.state.translation['Place']['Outside']}</a></span>
                </div>
                break;
            case 3:
                listLocation = <div className="scroll-animo-lugares">
                    <span><a className="lugar-pactica01" data-id="0" onClick={this.handleLocation}>{this.state.translation['Place']['Work']}</a></span>
                    <span><a className="lugar-pactica02" data-id="1" onClick={this.handleLocation}>{this.state.translation['Place']['Home']}</a></span>
                    <span><a className="lugar-pactica03" data-id="2" onClick={this.handleLocation}>{this.state.translation['Place']['Tergar']}</a></span>
                    <span><a className="lugar-pactica04-activo" data-id="3" onClick={this.handleLocation}>{this.state.translation['Place']['Outside']}</a></span>
                </div>
                break;

        }
        return (
            <div className="col-xs-12 col-sm-5 col-md-4 mt16">
                <div className="cont-sombra-redondeado">
                    <div className="head-naranja">
                        <div className="col-xs-12">
                            <h5>{this.state.translation["New session"]}</h5>
                        </div>
                    </div>
                    <div className="body-listados-detalle">

                            <label className="mt32">{this.state.translation["Course"]}</label>
                            <ListCourse newPractice={this.getPractice} selectedPractice = {this.state.selectedPractice} />

                            <div className="clearfix"></div>

                            <div className="campo50 mb16 f-left">
                                <label>Date</label>
                                <div className="input-group date form_datetime">
                                    <DatePicker selected={this.state.date} onChangeRaw={(e)=>this.handleChangeRaw(e)} onChange={this.handleDate} id="example-datepicker" className="form-control" style={{width: "100%"}} dateFormat="DD/MM/YYYY"  maxDate={moment()}/>
                                    <span className="input-group-addon"><span
                                        className="glyphicon glyphicon-calendar"></span></span>
                                </div>

                            </div>

                            <div className="campo50 mb16 f-right">
                                <label>Time</label>
                                <div className="input-group">
                                    <TimePicker className="form-control" onChange={this.handleTime} showSecond={false} value={this.state.elapsed}/>
                                    <span className="input-group-addon"><i className="glyphicon glyphicon-time"></i></span>
                                </div>
                            </div>

                            <div className="clearfix"></div>

                            <div style={{display: display}}>
                                <div className="campo30 f-left  mb16">
                                    <label> Beads per mala<span className="ocultar"></span></label>
                                    <input className="form-control  text-center" defaultValue={this.state.baseBeads}  type="number" min="7" max="216" onChange={this.changeBaseBeads}/>
                                </div>

                                <div className="f-left signo  mb16">X</div>

                                <div className="campo30 f-left mb16">
                                    <label>Malas</label>
                                    <input className="form-control text-center" min="1" defaultValue={this.state.malaSize} type="number" onChange={this.changeMalas} />
                                </div>

                                <div className="f-left signo font16 mb16">=</div>

                                <div className="campo30 f-left mb16">
                                    <label>Beads</label>
                                    <input className="form-control solo-lectura text-center" defaultValue={this.state.beads} type="number" ref={this.beadsInput} readOnly />
                                </div>
                            </div>
                            <div className="clearfix"></div>

                            <div className="estado-animo-lugares mb16">
                                <label>Feelings</label>
                                {listFeeling}
                            </div>

                            <div className="clearfix"></div>

                            <div className="estado-animo-lugares mb32">
                                <label>Place</label>
                                {listLocation}
                            </div>

                            <div className="clearfix"></div>

                            <div className="campo50 mb32 f-left">
                                <button className="guardar" onClick={this.saveSession}>
                                    <img src={btn_save} alt="Add Session" />
                                        <hr /><span>{this.state.translation['Save']}</span>
                                </button>
                            </div>

                            <div className="campo50 mb32 f-right">
                                <button className="eliminar" onClick={this.cancelSession}>
                                    <img src={btn_cancel} alt="Add Session" />
                                        <hr /><span>{this.state.translation['Cancel']}</span>
                                </button>
                            </div>
                    </div>
                </div>
                {!this.state.isHiddenMessageCancel && <MessageCancel changeHiddenMessageCancel = {this.changeHiddenMessageCancel} changeToClose = {this.changeToClose}/>}
                {!this.state.isHiddenMessageSaved && <MessageSaved/>}
            </div>
        );
    }
}

const mapStateToProps =  state =>
{
    return{

    }
}
const mapDispatchToProps = dispatch =>
{
    return {
        createSession (course,ismala)
        {
            dispatch(createSession(course,ismala));
        }
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(NewMeditationSession);