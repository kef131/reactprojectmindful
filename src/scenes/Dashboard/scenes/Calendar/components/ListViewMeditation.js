import React, { Component } from 'react'
import DetailsMeditation from './EditMeditation';
import dateFns from 'date-fns';
import moment from 'moment';
import Language from "../../../../../components/Language";
import emotion1 from '../../../../../img/icon/emotion1.svg';
import emotion2 from '../../../../../img/icon/emotion2.svg';
import emotion3 from '../../../../../img/icon/emotion3.svg';
import emotion4 from '../../../../../img/icon/emotion4.svg';
import place1 from '../../../../../img/icon/place1.svg';
import place2 from '../../../../../img/icon/place2.svg';
import place3 from '../../../../../img/icon/place3.svg';
import place4 from '../../../../../img/icon/place4.svg';
import {Scrollbars} from 'react-custom-scrollbars';
import {
    ContenedorSombraRedondeado,
    CuerpoListadoDetalle,
    DiaEncabezado, ElementoListadoPracticaDuracion, ElementoListadoPracticaImg, ElementoListadoPracticaMalas,
    EncabezadoNaranja,
    FechaEncabezado, TituloListadoPractica
} from "../styles/CalendarStyle";
export default class ListViewMeditation extends Component
{

    constructor()
    {
        super();

        let language = localStorage.getItem("language").toLocaleLowerCase();

        this.locale = require('date-fns/locale/'+language);

        this.state = {
            translation: Language[localStorage.getItem("language")],
            idPractice: 0,
            isMalaSession: false,
            isHiddenDetails: true,
            isHiddenListView: true,
        }


        this.handleEdit = this.handleEdit.bind(this);
    }


    handleEdit(e)
    {
        //Find information about practice.
        //const data = this.state.data.filter(data => data.id === e.currentTarget.dataset.id);

        let isMala  = (e.currentTarget.dataset.ismala == "true") ? true : false;
        this.setState({idPractice: e.currentTarget.dataset.id, isMalaSession: isMala, isHiddenDetails: false, isHiddenListView: false });

    }

    secondsToHms = (d) => {
        const time = new Date(null);

        time.setSeconds(d);
        const stringTime = time.toISOString().substr(11,8);

        return stringTime;
    }

    switchFeeling = (feeling) =>
    {
        switch (feeling) {
            case "DISTRACTED":
            case 0:
                return <ElementoListadoPracticaImg src={emotion2} />;

            case "HAPPY":
            case 1:
                return <ElementoListadoPracticaImg src={emotion1} />;

            case "BORED":
            case 2:
                return <ElementoListadoPracticaImg src={emotion3} />;

            case "NORMAL":
            case 3:
                return <ElementoListadoPracticaImg src={emotion4} />;
        }
    }

    switchPlace = (place) =>
    {
        switch (place) {
            case "WORK":
            case 0:
                return <ElementoListadoPracticaImg src={place1} />;

            case "HOME":
            case 1:
                return <ElementoListadoPracticaImg src={place2} />;

            case "TERGAR":
            case 2:
                return <ElementoListadoPracticaImg src={place3} />;

            case "OUTSIDE":
            case 3:
                return <ElementoListadoPracticaImg src={place4} />;
        }
    }

    getBackPressButton = (isHidden) =>
    {
        (isHidden == "true") ? this.setState({isHiddenDetails: true, isHiddenListView: true}) : '';
    }

    changeHiddenEdit = (hidden) =>
    {
        this.setState({isHiddenDetails: true, isHiddenListView: hidden});
    }
    render() {

        //Sort the data
        let data = this.props.data;

        return (
                <div>
                    {(this.state.isHiddenListView) &&
                    <div className="col-xs-12 col-sm-5 col-md-4 mt16">
                        <ContenedorSombraRedondeado>
                            <EncabezadoNaranja >
                                <FechaEncabezado>{dateFns.format(this.props.selectedDate, 'dddd', {locale: this.locale})}<br/><DiaEncabezado>{dateFns.format(this.props.selectedDate, 'MMM DD',{locale: this.locale})}</DiaEncabezado>
                                </FechaEncabezado>
                            </EncabezadoNaranja>

                            {/*<Scrollbars style={{height: 532}}>*/}
                            <CuerpoListadoDetalle >
                                <div className="scroll-listado-detalle">
                                            {data.map((data) => {
                                                    if (moment(data.date).utc().format("YYYY-MM-DD") === dateFns.format(this.props.selectedDate, 'YYYY-MM-DD')) {
                                                    return (
                                                            <div key={data.id}>
                                                                <div className="listado-practicas mt32"
                                                                     onClick={this.handleEdit} data-id={data.id} data-ismala={data.course.is_mala_course}>
                                                                    <TituloListadoPractica>{this.state.translation[data.course.name]}</TituloListadoPractica>
                                                                    <ElementoListadoPracticaDuracion>{this.secondsToHms(data.elapsed)} </ElementoListadoPracticaDuracion>

                                                                    {(data.course.is_mala_course) ?
                                                                        <ElementoListadoPracticaMalas>{data.beads} beads
                                                                            / {parseInt(data.beads / data.malaSize)} malas</ElementoListadoPracticaMalas> : ""}
                                                                    <div className="clearfix"></div>
                                                                    {this.switchFeeling(data.feeling)}
                                                                    {this.switchPlace(data.place)}
                                                                </div>
                                                                <div className="clearfix"></div>
                                                            </div>
                                                        )
                                                    }
                                                }
                                            )}
                                </div>
                            </CuerpoListadoDetalle>
                            {/*</Scrollbars>*/}
                        </ContenedorSombraRedondeado>
                    </div>
                    }
                    {(!this.state.isHiddenDetails) &&
                        <DetailsMeditation idPractice={this.state.idPractice} ismala={this.state.isMalaSession} onBackPressButton={this.getBackPressButton} isHiddenEdit={this.changeHiddenEdit}/>}
                </div>
            );
    }
}