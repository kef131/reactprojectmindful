import React, {Component} from "react";
import dateFns from "date-fns";
import ListView from './components/ListViewMeditation';
import NewSession from './components/NewMeditationSession';
import {connect} from 'react-redux';
import {loadMalaMeditationSession, filterPractices} from "../../services/actionCreators";
import FilterByCourse from "./components/FilterByCourse";
import moment from "moment";
import Language from "../../../../components/Language";
import arrow_l from '../../../../img/icon/arrow-l.svg';
import arrow_r from '../../../../img/icon/arrow-r.svg';
import plus from '../../../../img/icon/plus.svg';
import {
    Anio,
    AnioMes,
    BotonMes,
    ContenedorSombraRedondeado,
    CuerpoCalendario,
    ElementoMes,
    EncabezadoCalendario,
    EncabezadoCalendarioMovil,
    EncabezadoCalendarioPC,
    EncabezadoNaranja,
    FlechaDerecha,
    FlechaIzquierda,
    ListaMeses,
    RenglonCalendario,
    TablaCalendario
} from "./styles/CalendarStyle";
import {BtnBcoCalendar} from "../../styles/Buttons";

class Calendar extends Component {

    constructor(props)
    {
        super(props);

        if(!("practiceCode" in localStorage) && !("practiceType" in localStorage)){
            localStorage.setItem('practiceCode','All');
            localStorage.setItem('practiceType','All courses');
        }

        let language = localStorage.getItem("language").toLocaleLowerCase();

        this.locale = require('date-fns/locale/'+language);

        console.log(this.locale);
        this.state = {
            translation: Language[localStorage.getItem("language")],
            currentDate: new Date(),
            selectedDate: new Date(),
            isClickDay : false,
            isHiddenListView: false,
            isHiddenAddSession: true,
            practiceName: localStorage.getItem("practiceType"),
            practiceCode: localStorage.getItem("practiceCode"),
        };


        this.handleCourse = this.handleCourse.bind(this);
    }

    //Render Calendar Header.
    renderHeader() {
        return (
            <EncabezadoNaranja>
                {/*Date and button to change month*/}
                <div className="col-xs-9 col-sm-5">
                    <FlechaIzquierda onClick={this.prevYear}/>
                    <Anio>{dateFns.format(this.state.currentDate,'YYYY')}</Anio>
                    <AnioMes>{dateFns.format(this.state.currentDate,'MMMM YYYY')}</AnioMes>
                    <FlechaDerecha onClick={this.nextYear}/>
                </div>
                {/*Button to add session*/}
                <div className="col-xs-3 col-sm-7 text-right">
                    <BtnBcoCalendar onClick={this.onNewSessionClick}>{this.state.translation['Add session']}</BtnBcoCalendar>
                </div>
            </EncabezadoNaranja>
        );
    }

    //Render <li> of Months.
    renderMonths()
    {
        const dateFormat = "MMM";
        const months = [];

        for (let i = 0; i <= 11; i++) {
            months.push(
                //TODO months selected
                //How this works?
                <ElementoMes key={i} >
                    <BotonMes className={
                        (dateFns.getMonth(this.state.currentDate) == dateFns.getMonth(new Date().setMonth(i)))
                            ? "mes-activo"
                            : ""
                        }
                       onClick={() => this.onClickMonth(dateFns.getMonth(new Date().setMonth(i)))}
                    >
                        {dateFns.format(new Date(null,i), dateFormat, {locale : this.locale})}
                    </BotonMes>
                </ElementoMes>)
        }
        return (
            <ListaMeses className="hidden-xs">
                {months}
                <div className="clearfix"></div>
            </ListaMeses>
        );
    }

    //Render Day Label of Week.
    renderDays() {
        const days = [];

        let startDate = dateFns.startOfWeek(this.state.currentDate);

        for (let i = 0; i < 7; i++) {
            days.push(
                <EncabezadoCalendario scope="col" key={i}>
                    <EncabezadoCalendarioMovil> {dateFns.format(dateFns.addDays(startDate, i), "dd",{locale: this.locale})} </EncabezadoCalendarioMovil>
                    <EncabezadoCalendarioPC> {dateFns.format(dateFns.addDays(startDate, i), "ddd", {locale: this.locale})} </EncabezadoCalendarioPC>
                </EncabezadoCalendario>
            );
        }
        return <RenglonCalendario>{days}</RenglonCalendario>;
    }

    //Render table of Days of week.
    renderCells() {
        let { currentDate, selectedDate } = this.state;

        //selectedDate = (dateFns.format(this.state.selectedDate,'YYYY-MM-DD') === this.props.selectedDate) ? new Date(this.state.selectedDate) : new Date(this.props.selectedDate);

        const monthStart = dateFns.startOfMonth(currentDate);
        const monthEnd = dateFns.endOfMonth(monthStart);
        const startDate = dateFns.startOfWeek(monthStart);
        const endDate = dateFns.endOfWeek(monthEnd);

        const dateFormat = "D";
        const rows = [];

        let days = [];
        let day = startDate;
        let formattedDate = "";

        //While the current day doesn't come to the end of month
        while (day <= endDate) {
            for (let i = 0; i < 7; i++) {
                formattedDate = dateFns.format(day, dateFormat);
                const cloneDay = day;
                days.push(
                    //Todo Change the selected day with props
                    //Options: DiaDesactivado / DiaSesion / DiaNormal / DiaNoSeleccionable
                    <td
                        className={`${
                            (!dateFns.isSameMonth(day, monthStart))
                                ? "dia-desactivado"
                                : dateFns.isSameDay(day, selectedDate) ? "dia-seleccionado" : this.verifyDay(day)
                            }`}
                        key={day}
                        onClick={() => ((dateFns.isSameMonth(cloneDay, monthStart) ? this.onDateClick(dateFns.parse(cloneDay)) : null) )}
                    >
                        <span className="">{formattedDate}</span>
                    </td>
                );
                day = dateFns.addDays(day, 1);
            }
            rows.push(
                <RenglonCalendario key={day}>
                    {days}
                </RenglonCalendario>
            );
            days = [];
        }
        return rows;
    }

    //Click Event when I selected a day of month.
    onDateClick = day => {
        this.setState({
            selectedDate: day,
            isClickDay: true,
            isHiddenListView: false,
            isHiddenAddSession: true,
        });
    };

    onNewSessionClick = () =>
    {
        this.setState({
            isClickDay: false,
            isHiddenListView: true,
            isHiddenAddSession: false,
        })
    }
    //Verify if exists a day with practices.
    verifyDay = date =>
    {
        let data = this.props.data.filter( d => (moment(d.date).utc().format("YYYY-MM-DD") ===  dateFns.format(date,'YYYY-MM-DD')) /*console.log(dateFns.format(date, 'YYYY-MM-DD'))*/ );
        //"existe-sesion"
        return (data.length > 0) ? 'existe-sesion' :  '';
    }

    //+1 year.
    nextYear = () => {

        //If stored year isn't greater than this current year.
        if(dateFns.getYear(this.state.currentDate) < dateFns.getYear(new Date()))
        {
            this.setState({
                currentDate: dateFns.addYears(this.state.currentDate,1)
            });
        }
    }

    //-1 year.
    prevYear = () => {
        this.setState({
            currentDate: dateFns.subYears(this.state.currentDate,1)
        })
    }

    //When I changed month.
    onClickMonth = (month) => {
        this.setState({
            currentDate: dateFns.setMonth(this.state.currentDate, month)
        })
    };

    //When I chose a course of dropdown list.
    handleCourse = (e) =>
    {
        this.setState({course: e.currentTarget.dataset.id});

        this.getSessionByCourse(e.currentTarget.dataset.code);

    }

    getSessionByCourse =  (codeCourse) => {

        //Get prop.
        let data = this.props.data;

        //Get sessions by course that I chose.
        let sessionsCourse = (codeCourse == 'All') ? data : data.filter((session) => ((session.course.code == codeCourse) && (dateFns.format(session.date,'YYYY-MM-DD') === dateFns.format(this.state.selectedDate,'YYYY-MM-DD'))) );

        this.setState ({
            data: sessionsCourse,
        });
    }

    changeHiddenAddSession = (hidden) =>
    {
        this.setState({isHiddenAddSession : hidden});
    }

    changeHiddenListView = (hidden) =>
    {
        this.setState({isHiddenListView: hidden})
    }

    //Get practice from ListViewPractice
    getPractice = (practice) =>
    {
        practiceCode = practice.code;
        localStorage.setItem("practiceType", practice.name);
        localStorage.setItem("practiceCode", practice.code);
        this.setState({practiceName: practice.name, practiceCode: practice.code})
    }

    render() {

        //const newSelectedDate = (this.state.isClickDay) ? dateFns.format(this.state.selectedDate,'YYYY-MM-DD'): (dateFns.format(this.state.selectedDate,'YYYY-MM-DD') === this.props.selectedDate) ? dateFns.format(this.state.selectedDate,'YYYY-MM-DD'): this.props.selectedDate;
        return (
            <div className="col-xs-12 col-sm-7 col-md-8">
                <ContenedorSombraRedondeado>
                        {this.renderHeader()}
                        <CuerpoCalendario>
                            {this.renderMonths()}
                            <TablaCalendario>
                                {this.renderDays()}
                                {this.renderCells()}
                            </TablaCalendario>
                        </CuerpoCalendario>
                    </ContenedorSombraRedondeado>

                {!this.state.isHiddenListView && <ListView data = {this.props.data} selectedDate = {this.state.selectedDate} isHiddenListView={this.state.isHiddenListView} />}
                {!this.state.isHiddenAddSession && <NewSession changeHidden={this.changeHiddenAddSession}/>}
            </div>
        );
    }
}

let practiceCode = localStorage.getItem("practiceCode");
const mapStateToProps = state =>
{
    let data = [...state.dataMeditation,...state.dataMalaMeditation], newSelectedDate = '';

    if( practiceCode == null) practiceCode = 'All';

    //Ordering DESC
    data = data.sort((a,b) => Number(a.date) - Number(b.date) );

    data = (practiceCode == 'All') ? data : data.filter(data => data.course.code === practiceCode );
    //Check if there are meditation sessions.
    /*if(data.length !== 0) {
        newSelectedDate = dateFns.format(data[data.length-1].date,"YYYY-MM-DD");
    }*/

    return {
        data,
        selectedDate : newSelectedDate,
    };
};

const mapDispatchToProps = dispatch =>
{
    return {
        filterByPractice (code)
        {
            //dispatch(loadMeditationSession());
            //dispatch(loadMalaMeditationSession());
            dispatch(filterPractices(code));
        }
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(Calendar);