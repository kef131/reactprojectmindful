import React, {Component} from 'react';
import FilterByCourse from './components/FilterByCourse';
import Chart from 'react-google-charts';

export default class Statistic extends Component
{

    constructor()
    {
        super();

    }

    secondsToHms = (s) => {

        //time.setSeconds(d);
        var h = Math.floor(s / 3600);
        var m = Math.floor(s % 3600 / 60);

        var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";


        return hDisplay + mDisplay;
    }


    totalSessionTime = (data) => {

        let total = 0;
        data.map((data) => {

            total += data.elapsed;
        });

        return this.secondsToHms(total);
    }

    totalBeadsMala = (data) => {
        let totalBeads = 0, totalMalas = 0;

        data.map( (data) =>
        {
            totalBeads += data.beads;
            totalMalas += data.malaSize;
        });

        return {totalBeads, totalMalas};
    }

    averageBeadsBySession = (totalBeads,totalSessions) =>
    {
        return Math.round(totalBeads/totalSessions);
    }

    DateBeadsByAll = (data) => {

        let jsonBeads = [['Year','Beads']];
        let lastDay = '', nextDay = '';
        let lastData = data.length-1;
        let noBeads = 0;
        let elapsedTotal = 0;

        //let ddddd = new Chart
        for (let i = 0; i <= lastData; i++)
        {
            if(i === lastData)
            {
                nextDay = lastDay = new Date(data[i].date).toISOString().substr(0,10);

                noBeads += data[i].beads;
                //jsonBeads += "{" +lastDay.toString()+","+noBeads+"}";
            }
            else
                {
                lastDay = new Date(data[i].date).toISOString().substr(0,10);
                nextDay = new Date(data[i+1].date).toISOString().substr(0,10);

                if((lastDay === nextDay))
                {
                    elapsedTotal++;
                    noBeads += data[i].beads;
                }
                else
                {
                    noBeads += data[i].beads;
                    //jsonBeads[lastDay] = noBeads;

                    jsonBeads.push([lastDay.toString() , noBeads]);
                    noBeads = 0;
                }


            }
        }

        return jsonBeads;
    }

    totalSessions = (data) => { return data.length }

    render() {


        const {totalBeads, totalMalas} = this.totalBeadsMala(this.props.dataMala);
        const totalSessions = this.totalSessions(this.props.data);

        return (
            <div>
                <FilterByCourse/>
                ..
                <div className="col-xs-12 col-sm-12 ">
                <Chart
                    width={'1000px'}
                    height={'500px'}
                    chartType="LineChart"
                    loader={<div>Loading Chart</div>}
                    data={this.DateBeadsByAll(this.props.dataMala)}
                    options={{
                        title: '',
                        vAxis: { title: 'Count' },
                        hAxis: { title: 'Years' },
                        explorer: {
                            actions: ['dragToZoom', 'rightClickToReset'],
                            axis: 'horizontal',
                            keepInBounds: true,
                            maxZoomIn: 7.0
                        }
                    }}
                />
                </div>
                <div>
                    <div>
                        <span>Tiempo de práctica total: </span>
                        {this.totalSessionTime(this.props.data)}
                        <br />
                        <span>Total beads: </span>
                        { totalBeads }  beads/ {totalMalas} malas
                        <br />
                        <span># sessions: </span>
                        { totalSessions }
                        <br />
                        <span>Promedio de beads x session : </span>
                        { this.averageBeadsBySession(totalBeads,totalSessions)}
                    </div>
                </div>
            </div>
        );
    }
}