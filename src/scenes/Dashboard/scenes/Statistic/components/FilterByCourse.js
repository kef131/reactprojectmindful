import React, {Component} from 'react';


export default class FilterByCourse extends Component
{
    render() {
        return (
            <div className="col-xs-12 col-sm-6 text-right filtrar-calendario">
                <div className="dropdown">
                    <label>Filter by:</label>
                    <button className="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Course
                        <span className="caret"></span>
                    </button>
                    <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li data-id="All courses" data-code="All" onClick={this.handleCourse}><a href="">All courses</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="Joy of Living 1" data-code="JOL1" onClick={this.handleCourse}><a href="">Joy of Living 1</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="Joy of Living 2" data-code="JOL2" onClick={this.handleCourse}><a href="">Joy of Living 2</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="Joy of Living 3" data-code="JOL3" onClick={this.handleCourse}><a href="">Joy of Living 3</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="Guru Yoga" data-code="GURU_YOGA" onClick={this.handleCourse}><a href="">Guru Yoga</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="Nature of mind" data-code="NOM" onClick={this.handleCourse}><a href="">Nature of mind</a></li>
                        <li role="separator" className="divider"></li>
                        <li  data-id="Vajrasattva" data-code="VAJRASATTVA" onClick={this.handleCourse}><a href="">Vajrasattva</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="Prostrations" data-code="PROSTRATIONS" onClick={this.handleCourse}><a href="">Prostrations</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="Mandala" data-code="MANDALA" onClick={this.handleCourse}><a href="">Mandala</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="Tara" data-code="TARA" onClick={this.handleCourse}><a href="">Tara</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="Nectar of the Path" data-code="NECTAR_PATH" onClick={this.handleCourse}><a href="">Nectar of the Path</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="White Tara Long" data-code="WHITE_TARA_LONG" onClick={this.handleCourse}><a href="">White Tara Long</a></li>
                        <li role="separator" className="divider"></li>
                        <li data-id="Mindfulness" data-code="MINDFULNESS" onClick={this.handleCourse}><a href="">Mindfulness</a></li>
                    </ul>
                </div>
            </div>
        );
    }
}