import React, {Component} from 'react';
import ListCourse from './components/ListCourse';
import CountDown from './components/CountDown';
//import ReactNotifications from 'react-browser-notifications';
import moment from 'moment';
import Language from "../../../../components/Language";
//import {TituloMensaje, CajaMensaje, IconotTitulo, TextMensaje, TextTitulo} from './styles/Messages';
import {
    TimerWrapper,
    BellsWrapper,
    WhiteBox,
    BtnReset,
    BtnPlay,
    TimerInput,
    TimerPractice,
    ClearFix,
    TimerContainer,
    ColonLabel, TimerButton, PlusBell, InputBell, MinusBell
} from './styles/TimerStyle';
import iconMusic from '../../../../img/icon/iconmusic.png';
import iconPDF from '../../../../img/icon/iconpdf.png';
import audio_start from '../../../../audio/start.mp3';
import Audio from '../../../../components/Multimedia';
import ReactPlayer from "react-player";

class Timer extends Component
{
    constructor()
    {
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
            hours: 0,
            minutes: 1,
            seconds: 0,
            originalDate: new Date(),
            olderDate: new Date(),
            newDate: new Date(),
            isHiddenCountDown : true,
            isHiddenReminders: true,
            isHiddenButtonReset: false,
            isHiddenButtonPlay: false,
            selectedPractice: 'Joy of Living 1',
            idPractice : 1,
            isCheckedAudio: false,
            isCheckedPDF: false,
            multimedia: Audio[localStorage.getItem("language")],
            selectedSong: null,
            selectedPDF : null,
        }
        this.handleHours = this.handleHours.bind(this);
        this.handleMinutes = this.handleMinutes.bind(this);
        this.handleSeconds = this.handleSeconds.bind(this);
        this.handleCheckboxAudio = this.handleCheckboxAudio.bind(this);
        this.handleCheckboxPDF = this.handleCheckboxPDF.bind(this);
        this.handleClickAudio = this.handleClickAudio.bind(this);
        this.handleClickPDF = this.handleClickPDF.bind(this);
        this.playTimer = this.playTimer.bind(this);
        this.resetTimer = this.resetTimer.bind(this);

    }

    //Get value
    getPractice = (practice) =>
    {
        this.setState({selectedPractice: practice.name, idPractice : parseInt(practice.id)});
    }

    //Get hours input.
    handleHours = (e) =>
    {
        let hours = parseInt(e.currentTarget.value);
        let minutes, seconds;

        hours = (hours > 3 || hours < 0  || isNaN(hours)) ?  3 : hours;
        minutes = (hours == 3) ? 0: this.state.minutes;
        seconds = (hours == 3 ) ? 0 : this.state.seconds;

        this.setState({hours,minutes,seconds});
    }

    //Get minutes input.
    handleMinutes = (e) =>
    {
        let minutes = parseInt(e.currentTarget.value);

        minutes = (minutes > 59 || minutes < 0  || isNaN(minutes)) ?  1 : (this.state.hours == 3) ? 0 : minutes;
        //minutes = (minutes > 59 || minutes < 1  || isNaN(minutes)) ?  0 : minutes;

        this.setState({minutes});
    }

    //Get seconds input.
    handleSeconds = (e) =>
    {
        let seconds = parseInt(e.currentTarget.value);

        seconds = ((seconds > 59 || seconds < 0  || isNaN(seconds)) /*&& this.state.minutes >= 1*/ ) ?  59 : seconds;

        this.setState({seconds});
    }

    //Start timer.
    playTimer = () =>
    {
        if( this.state.hours <= 0 && this.state.minutes <= 0 ){
            alert( "The minimum session time is 1 minute. Please insert a valid duration" );
            return;
        }

        let olderDate = new Date();
        let newDate = moment(olderDate).clone().add(this.state.hours,'h').add(this.state.minutes,'m').add(this.state.seconds,'s');

        localStorage.setItem("time",JSON.stringify({start: moment(olderDate).format("MM/DD/YYYY HH:mm:ss"), end: moment(newDate).format("MM/DD/YYYY HH:mm:ss")}));
        document.getElementById('play').play();

        //Hidden scroll.
        document.documentElement.style.overflow = 'hidden';
        document.body.scroll = "no";

        this.setState({isHiddenCountDown : false,
            originalDate: moment(olderDate).format("MM/DD/YYYY HH:mm:ss"),
            olderDate: moment(olderDate).format("MM/DD/YYYY HH:mm:ss"),
            newDate: moment(newDate).format("MM/DD/YYYY HH:mm:ss"),
            isHiddenButtonReset: true,
            isHiddenButtonPlay: true,
        });
    }

    //Reset values of timer.
    resetTimer = () =>
    {
        this.setState({hours: 0, minutes: 1, seconds: 0, isHiddenCountDown : true, isCheckedAudio: false, isCheckedPDF: false, selectedSong: null, selectedPDF: null });
    }

    //Timer Design Format.
    addLeadingZeros(value) {
        value = String(value);
        while (value.length < 2) {
            value = '0' + value;
        }
        return value;
    }


    /********************** Song *****************************/
    /*onDuration = (durationSong) => {
        console.log('onDuration', durationSong);
        console.log('onDuration round', Math.round(durationSong));
        alert(Math.round(durationSong));
        console.log('timer', this.secondsToHMS(Math.round(durationSong)));
        this.setState({ durationSong });
    }*/

    secondsToHMS = (s) =>
    {
        const time = {
            hours: 0,
            minutes: 0,
            seconds: 0,
        };

        // calculate time difference between now and expected date
        if (s >= 3600) { // 60 * 60 = 3600 seconds = 1 hour
            time.hours = Math.floor(s / 3600);
            s -= time.hours * 3600;
        }
        if (s >= 60) { // 60 seconds = 1 minute
            time.minutes = Math.floor(s / 60);
            s -= time.minutes * 60;
        }
        time.seconds = s;

        return time;
    }

    handleCheckboxAudio = (event) => {
        const target = event.target;
        const value = target.checked;
        let selectedSong, durationSong;
        let hours, minutes, seconds;

        //It's checked input.
        if(value)
        {
            selectedSong = event.currentTarget.dataset.song;
            durationSong = parseFloat(event.currentTarget.dataset.duration);
            const hms = this.secondsToHMS(Math.round(durationSong));
            hours = hms.hours;
            minutes = hms.minutes;
            seconds = hms.seconds;
        }
        else
        {
            selectedSong = null;
            hours = 0;
            minutes = 1;
            seconds = 0;
        }


        this.setState({
            isCheckedAudio : value,
            selectedSong,
            hours,
            minutes,
            seconds,
        });
    }

    handleCheckboxPDF = (event) => {
        const target = event.target;
        const value = target.checked;
        let selectedPDF = (value) ? event.currentTarget.dataset.pdf : null;

        this.setState({
            isCheckedPDF : value,
            selectedPDF
        });
    }

    handleClickAudio = (event) => {

        let selectedSong, durationSong, value = event.currentTarget.dataset.checked;
        let hours, minutes, seconds;

        //It's checked input.
        if(value == "false")
        {
            selectedSong = event.currentTarget.dataset.song;
            durationSong = parseFloat(event.currentTarget.dataset.duration);
            const hms = this.secondsToHMS(Math.round(durationSong));
            hours = hms.hours;
            minutes = hms.minutes;
            seconds = hms.seconds;
            value = true;
        }
        else
        {
            selectedSong = null;
            hours = 0;
            minutes = 1;
            seconds = 0;
            value = false;
        }


        this.setState({
            isCheckedAudio : value,
            selectedSong,
            hours,
            minutes,
            seconds,
        });
    }

    handleClickPDF = (event) => {
        let value = event.currentTarget.dataset.checked;
        let selectedPDF;

        if(value == "false")
        {
            value = true;
            selectedPDF = event.currentTarget.dataset.pdf;
        }
        else
        {
            value = false;
            selectedPDF = null;
        }

        this.setState({
            isCheckedPDF : value,
            selectedPDF
        });
    }

    render() {
        return (
            <div className="col-xs-12" id="timer">
                <TimerWrapper>
                <WhiteBox>
                    <TimerPractice>
                        <audio id='play' src={audio_start} ></audio>
                        <label>{this.state.translation['Select a practice']}</label>
                        <ListCourse typeList="Meditation" selectedPractice={this.state.selectedPractice} newPractice={this.getPractice}/>
                        <ClearFix/>
                        <TimerContainer>
                            <TimerInput value={this.addLeadingZeros(this.state.hours)} type="text" onChange={this.handleHours} disabled={this.state.isCheckedAudio} />
                            <label>{this.state.translation['Hours']}</label>
                        </TimerContainer>
                            <ColonLabel>:</ColonLabel>
                        <TimerContainer>
                            <TimerInput value={this.addLeadingZeros(this.state.minutes)} type="text" onChange={this.handleMinutes} disabled={this.state.isCheckedAudio}/>
                            <label>{this.state.translation['Minutes']}</label>
                        </TimerContainer>
                            <ColonLabel>:</ColonLabel>
                        <TimerContainer>
                            <TimerInput value={this.addLeadingZeros(this.state.seconds)} type="text" onChange={this.handleSeconds} disabled={this.state.isCheckedAudio}/>
                            <label>{this.state.translation['Seconds']}</label>
                        </TimerContainer>
                        <ClearFix />
                    </TimerPractice>
                    <TimerButton>
                        {/*<BellsWrapper>
                            <label>Random notification</label>
                            <ClearFix />
                            <PlusBell>+</PlusBell>
                            <InputBell defaultValue={10} />
                            <MinusBell>-</MinusBell>
                            <ClearFix/>
                        </BellsWrapper>*/}
                        {!this.state.isHiddenButtonPlay && <BtnPlay onClick={this.playTimer} />}
                        {!this.state.isHiddenButtonReset && <BtnReset onClick={this.resetTimer} />}
                    </TimerButton>
                </WhiteBox>
                    {this.state.selectedPractice == "Nectar of the Path" &&
                        <WhiteBox>
                            {/*<ReactPlayer url={this.state.multimedia[this.state.selectedPractice].srcSong} width="0" height="0" onDuration={this.onDuration}/>*/}
                            <audio src={this.state.multimedia[this.state.selectedPractice].srcSong} id="audio1"></audio>
                            <label>{this.state.translation['Practice material']}:</label> <br/>
                            <img src={iconMusic} style={{width: "30px"}}/>
                            <label style={{cursor:"pointer"}} data-checked={this.state.isCheckedAudio} data-song ={this.state.multimedia[this.state.selectedPractice].srcSong} data-duration = {this.state.multimedia[this.state.selectedPractice].durationSong} onClick={this.handleClickAudio}>{this.state.multimedia[this.state.selectedPractice].name}</label>
                            <input style={{float:"right", margin: 0}} type="checkbox" name="isCheckedAudio" checked={this.state.isCheckedAudio} data-song ={this.state.multimedia[this.state.selectedPractice].srcSong} data-duration = {this.state.multimedia[this.state.selectedPractice].durationSong} onChange={this.handleCheckboxAudio}/>
                            <br />
                            <img src={iconPDF} style={{width: "30px"}}/>
                            <label style={{cursor:"pointer"}} data-checked={this.state.isCheckedPDF} data-pdf={this.state.multimedia[this.state.selectedPractice].srcPDF} onClick={this.handleClickPDF}>{this.state.multimedia[this.state.selectedPractice].name}</label>
                            <input style={{float:"right", margin: 0}} type="checkbox" name="isCheckedPDF" checked={this.state.isCheckedPDF} data-pdf={this.state.multimedia[this.state.selectedPractice].srcPDF} onChange={this.handleCheckboxPDF}/>
                        </WhiteBox>
                    }
                </TimerWrapper>

                    {!this.state.isHiddenCountDown && <CountDown newDate = {this.state.newDate} olderDate = {this.state.olderDate} practice = {this.state.idPractice} song = {this.state.selectedSong} pdf = {this.state.selectedPDF} />}
            </div>
        );
    }
}

export default  Timer;