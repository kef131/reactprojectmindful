import styled from "styled-components";
import btn_play from '../../../../../img/icon/btn-play.svg';
import btn_restablecer from '../../../../../img/icon/btn-restablecer.svg';

export const TimerWrapper = styled.div`
  width: 405px;
  margin-bottom: 80px;

  @media  (max-width: 767px){
    width: 100%;
  }
`;
export const BlackBack = styled.section`
  display: block;
  background: #000000;
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 999;
  left: 0;
  top: 0;
  
  @media  (max-width: 767px){
    
  }
`;

export const MarginTimer = styled.div`
    margin-top: 80px;
`
export const WhiteBox = styled.div`
  background-color: rgba(255, 255, 255, 0.9);
  box-shadow: 0 0 14px rgba(0, 0, 0, 0.17);
  box-sizing: border-box;
  padding: 16px;
  border-radius: 4px;
`;

export const PracticeFilter = styled.select`
  background-color: rgba(255,255,255,0.9);
  box-shadow: 0 0 14px rgba(0,0,0,0.17);
  box-sizing: border-box;
  padding:16px;
  width: 480px;
  border-radius:4px;
`;

export const TimerInput = styled.input`
  border: none;
  background: none;
  color: ${props => props.disabled ? '#e1e1e1' : '#727070' } ;
  font-size: 50px;
  font-weight: 400;
  width: 100%;
  text-align: center;
  line-height: 0;
  padding:0;
  -webkit-text-fill-color: ${props => props.disabled ? '#e1e1e1' : '#727070' } ;
  -webkit-opacity:1; 
  + label {
    font-family: 'Roboto', sans-serif;
    display: block;
    position: relative;
    text-align: center;
    }
`;

const BtnTimer = styled.button`
  background-repeat:no-repeat;
  border-radius: 200px;
  border: none;
  width: 48px;
  height: 48px;
  transition: all 0.3s ease;
  display: inline-block;
  vertical-align: middle;
`;
export const BtnPlay = styled(BtnTimer)`
  background-color:#ff9333;
  background-image: url(${btn_play});
  background-size: 28px;
  background-position: 12px center;
  
  &:hover{
    background-color: #5C6BC0;
  }
`;

export const BtnReset = styled(BtnTimer)`
  background-color:#e1e1e1;
  background-image: url(${btn_restablecer});
  background-size: 38px;
  background-position: 4px center;
  margin: 0 0 0 14px;
	
	&:hover{
    background-color: #5C6BC0;
    color:#fff;
`;

export const BellsWrapper = styled.div`
  width: 100%;
  margin-bottom:16px;

  @media  (max-width: 767px){
    width: 150px;
    display: inline-block;
    vertical-align: middle;
  }
`;

export const BtnPause = styled(BtnTimer)`
  background-color:#e1e1e1;
  background-image: url(${btn_restablecer});
  background-size: 38px;
  background-position: 4px center;
	
	&:hover{
    background-color: #5C6BC0;
`;

export const TimerContainer = styled.div`
  display: inline-block;
  width: 70px;
  overflow: hidden; 
  color: #727070;
`;

export const ColonLabel = styled.span`
  display: inline-block;
  width: 8px;
  text-align: center;
  overflow: hidden; 
  color: #5c6bc0;
  font-size:31px;
  vertical-align: top;
`;

export const TimerPractice = styled.div`
  width: 240px;
  display: inline-block;

  @media  (max-width: 767px){
    width: 100%;
    margin-bottom: 10px;
  }
`;

export const ClearFix = styled.div`
  clear: both;
`;

export const TimerButton = styled.div`
  width: 130px;
  display: inline-block;
  vertical-align: top;

  @media  (max-width: 767px){
    width: 100%;
  }
`;

const BtnBells = styled.button`
  border: 1px solid #d8d8d8;
  color: #727070
  float: left;
  font-size:20px;
  width: 32px;
  height: 32px;
  text-align: center;
  padding: 0;

	&:hover{
    background-color: #5C6BC0;
    color:#fff;
`;

export const PlusBell = styled(BtnBells)`
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  border-right: none;
`;

export const MinusBell = styled(BtnBells)`
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  border-left: none;
`;

export const InputBell = styled.input`
  float:left;
  border: 1px solid #d8d8d8;
  border-radius: 0;
  height: 32px;
  width: 50px;
  text-align: center;
`;

const TimerSpan = styled.span`
  font-size: 50px;
  text-align: center;
  color: #fff;
  width: 70px;
  display: inline-block;
  margin-bottom: 32px;
  position:relative;
  vertical-align: middle;
`;

export const TimerHours = styled(TimerSpan)`
  
  &:after {
      content: "${props => props.title }";
      position: absolute; 
      font-size: 11px;
      left: 0;
      right: 0;
      bottom: -4px;
    }
`;

export const TimerMinutes = styled(TimerSpan)`
  
  &:after {
      content: "${props => props.title }";
      position: absolute; 
      font-size: 11px;
      left: 0;
      right: 0;
      bottom: -4px;
    }
`;

export const TimerSeconds = styled(TimerSpan)`
  
  &:after {
      content: "${props => props.title }";
      position: absolute; 
      font-size: 11px;
      left: 0;
      right: 0;
      bottom: -4px;
    }
`;

export const TimerColon = styled.span`
    display: inline-block;
    color:#fff;
    font-size: 40px;
    vertical-align: top;
    padding-top: 4px;
`;

export const ExtraTimeLabel = styled.span`
    background-color: #fff;
    margin: 10px auto;
    display: table;
    color:#000;
    font-size: 16px;
    vertical-align: top;
    padding: 4px 10px;
    text-align: center;
    border-radius: 6px;
`;

export const  FileContainer = styled.div`
    width: 100%;
    height: 100vh;
    overflow-y: scroll;
    position: relative;    
`