import styled from "styled-components";
import clap from '../../../../../img/icon/clap.svg';

export const Congratz = styled.h2`
  color: #fff;
  font-size: 16px;
  font-weight: 300;
  text-align: center;
`;

export const CongratzTxt = styled.span`
  display: inline-block;
  vertical-align: middle;
`;

export const CongratzIcon = styled.span`
  background-image: url(${clap});
  background-size: contain;
  background-repeat: no-repeat;
  display: inline-block;
  height: 26px;
  margin-bottom: 5px;
  vertical-align: middle;
  width: 32px;
`;

export const TotalTime = styled.p`
  font-size: 20px;
  text-align: center;
  margin: 20px auto 0;
  color: #fff;
`;

export const TotalTimeLabel = styled.p`
  font-size: 12px;
  text-align: center;
  margin: 0 auto;
  color: #fff;
  margin-bottom: 32px;
`;

export const AddExtraTime = styled.input`
  + label {
    position: relative;
    color: #fff;
    font-weight: 300;
    padding-left: 5px;

  &:checked + label {
    color: #fff;
  }
`;

export const SessionNote = styled.textarea`
  font-size: 12px;
  margin: 16px auto 0;
  color: #fff;
  width: 324px;
  display: inherit;
  resize: none;
  border-radius: 4px;
  background-color: transparent;
  border: 1px solid #fafafa;
  padding: 6px 12px;
  height: 100px;
`;

export const BtnContainer = styled.div`
  width: 324px;
  display: inherit;
  margin: 0 auto 10px;
  text-align: center;
`;