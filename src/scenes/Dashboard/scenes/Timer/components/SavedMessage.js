import React, {Component} from 'react';
import {CajaMensaje, IconotTitulo, TextMensaje, TextTitulo, TituloMensaje} from "../../../styles/Messages";
import Language from "../../../../../components/Language";
import good from '../../../../../img/icon/good.svg';

export default class SavedMessage extends Component{

    constructor ()
    {
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
            redirect: false,
        }
    }

    render() {
        setInterval(() => this.setState({redirect: true}), 3000);
        if (this.state.redirect){
            window.location.href = `${process.env.BASE_URL}/dashboard`;
            localStorage.removeItem("message_status");
        }

        //if(localStorage.getItem("message_status") == "true")
        //{
            return (
                <div className="modal-delete">
                    <div className="container text-center modal-color">
                        <CajaMensaje>
                            <TituloMensaje>
                                <IconotTitulo>
                                    <img
                                        src={good}
                                        height="100%"
                                    />
                                </IconotTitulo>
                                <TextTitulo>{this.state.translation["Awesome"]}</TextTitulo>
                            </TituloMensaje>
                            <TextMensaje>
                                {this.state.translation["Session has been saved!"]} <br />
                                {this.state.translation["Check it in your practice diary."]}
                            </TextMensaje>
                        </CajaMensaje>
                    </div>
                </div>
            );
        /*}
        else
        {
            return (
                <div className="modal-delete">
                    <div className="container text-center modal-color">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="mensajes">
                                    <img src="img/icon/warning.svg" height="90" alt="delete" />
                                    <p className="mt16">Oppss! There was a problem!</p>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                    </div>
                </div>
            );
        }*/
    }
}