import React, { Component } from 'react';
//import {connect} from 'react-redux';
//import {removeSession} from '../../../services/actionCreators';
//import MessageSaved from "./MessageSaved";
import {TituloMensaje, CajaMensaje, IconotTitulo, TextMensaje, TextTitulo} from '../../../styles/Messages';
import { BtnNormal, BtnResaltado, ClearFix } from "../../../styles/Buttons";
import Language from "../../../../../components/Language";
import icon_delete from '../../../../../img/icon/delete.svg';

class DiscardMessage extends Component{

    constructor ()
    {
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
        }

        this.discardSession = this.discardSession.bind(this);
        this.cancelDiscard = this.cancelDiscard.bind(this);

    }

    discardSession = () =>
    {
        //let element = document.getElementsByClassName("modal-delete");
        //element[0].parentNode.removeChild(element[0]);
        //this.props.changeHiddenMessageDelete(true);
        //Redirect timer without save session.
        window.location = `${process.env.BASE_URL}/dashboard`;

    }

    cancelDiscard = () => {
        this.props.changeHidden();
        //this.props.removeSession(this.props.id,this.props.ismala);
    }
    render() {
        return (
            <div className="modal-delete">
                <div className="modal-color">
                    <CajaMensaje>
                        <TituloMensaje>
                            <IconotTitulo>
                                <img src={icon_delete} height="100%"/>
                            </IconotTitulo>
                            <TextTitulo>{this.state.translation['Discard this session']}</TextTitulo>
                        </TituloMensaje>
                        <TextMensaje>
                            {this.state.translation['This session will not be saved. Are you sure you want to discard it?']}
                        </TextMensaje>
                        <BtnNormal onClick={this.discardSession}>{this.state.translation['Discard']}</BtnNormal>
                        <BtnResaltado  onClick={this.cancelDiscard}>{this.state.translation['Cancel']}</BtnResaltado>
                        <ClearFix />
                    </CajaMensaje>
                </div>
                </div>
            );
    }
}


/*const mapDispatchToProps = dispatch =>
{
    return {
        removeSession (id,ismala)
        {
            dispatch(removeSession(id,ismala));
        }
    };
}*/

export default DiscardMessage;