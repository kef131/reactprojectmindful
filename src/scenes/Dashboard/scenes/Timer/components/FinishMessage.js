import React, { Component } from 'react';
//import {connect} from 'react-redux';
//import {removeSession} from '../../../services/actionCreators';
//import MessageSaved from "./MessageSaved";
import icon_delete from '../../../../../img/icon/delete.svg';
import btn_save from '../../../../../img/icon/btn-save.svg';
import btn_cancel from '../../../../../img/icon/btn-cancel.svg';

class FinishMessage extends Component{

    constructor ()
    {
        super();

        this.saveSession = this.saveSession.bind(this);
        this.cancelDiscard = this.cancelDiscard.bind(this);

    }

    saveSession = () =>
    {
        this.props.changeHiddenSaveSession(true);
    }

    cancelDiscard = () => {
        this.props.changeHidden();
        //this.props.removeSession(this.props.id,this.props.ismala);
    }
    render() {
        return (
            <div className="modal-delete">
                <div className="container text-center modal-color">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="mensajes">
                                <img src={icon_delete} height="90" alt="delete"/>
                                <h2>You still have time left!</h2>
                                <p>Are you sure you want to finish your session?</p>
                            </div>
                        </div>
                        <div className="col-xs-6">
                            <button className="guardar" onClick={this.saveSession}>
                                <img src={btn_save} alt="Discard Session"/>
                                <hr/>
                                <span>Yes</span>
                            </button>
                        </div>
                        <div className="col-xs-6">
                            <button className="eliminar" onClick={this.cancelDiscard}>
                                <img src={btn_cancel} alt="Follow Session"/>
                                <hr/>
                                <span>No</span>
                            </button>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
            </div>
        );
    }
}


/*const mapDispatchToProps = dispatch =>
{
    return {
        removeSession (id,ismala)
        {
            dispatch(removeSession(id,ismala));
        }
    };
}*/

export default FinishMessage;