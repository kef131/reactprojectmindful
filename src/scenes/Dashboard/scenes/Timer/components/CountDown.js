import React, {Component} from 'react';
import DiscardMessage from "./DiscardMessage";
import moment from 'moment';
//import FinishMessage from "./FinishMessage";
import SaveSessionMessage from "./SaveSessionMessage";
import SavedMessage from "./SavedMessage";
import {
    TimerHours,
    TimerMinutes,
    TimerSeconds,
    TimerColon,
    ExtraTimeLabel,
    BlackBack,
    MarginTimer, FileContainer
} from '../styles/TimerStyle'
import {BtnContinueSession, BtnFinishSession, BtnDiscart, BtnPauseSession} from '../../../styles/Buttons';
import bell2 from '../../../../../audio/bell2.mp3';
import Language from "../../../../../components/Language";
import ReactPlayer from 'react-player';

class CountDown extends Component
{
    constructor()
    {
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
            diff: 0,
            hours: 0,
            min: 0,
            sec: 0,
            start: false,
            extraMinutes: 0,
            extraSeconds: 0,
            isHiddenExtraTime: true,
            isHiddenPause: false,
            isHiddenPlay: true,
            isHiddenDiscard: true,
            isHiddenFinish: true,
            isHiddenDiscardExtraTime: true,
            isHiddenDiscardMessage: true,
            /*isHiddenFinishMessage: true,*/
            isHiddenSaveSession: true,
            isHiddenSavedMessage: true,
            elapsed: 0,
            playedSong: 0,
            durationSong: 0,
            isPlaySong: true,
        }

        this.discardTimer = this.discardTimer.bind(this);
        this.pauseTimer = this.pauseTimer.bind(this);
        this.playTimer = this.playTimer.bind(this);
        this.finishTimer = this.finishTimer.bind(this);
    }

    componentDidMount() {
        // update every second
        const time =  JSON.parse(localStorage.getItem("time"));
        if( ! this.state.start ){
            const start_date = this.calculateCountdown(time.end);
            this.setState(start_date);
            this.setState({start: true});
        }

        this.interval = setInterval(() => {
            if("time" in localStorage)
            {
                const time_in_interval =  JSON.parse(localStorage.getItem("time"));
                if(!this.state.isHiddenPause || !this.state.isHiddenExtraTime) {
                    //const date = this.calculateCountdown(this.props.date);
                    const date = this.calculateCountdown(time_in_interval.end);
                    date ? this.setState(date) : /*this.stop()*/ this.calculateExtraTime(this.props.newDate);
                }
            }
            else
            {
                window.location = `${process.env.BASE_URL}/dashboard`;
            }
        }, 1000);
    }

    /*componentWillUnmount() {
        this.stop();
    }*/

    discardTimer = () =>
    {
        //this.stop();
        if(this.state.isHiddenExtraTime)
        {
            this.setState({
                isHiddenPause: true,
                isHiddenPlay: false,
                isHiddenDiscard: false,
                isHiddenFinish: false,
                isHiddenDiscardMessage : false,
            });
        }
        else
        {

            this.setState({
                isHiddenPause: true,
                isHiddenPlay: true,
                isHiddenDiscard: false,
                isHiddenFinish: false,
                isHiddenDiscardMessage : false,
            });
            /*isHiddenDiscardExtraTime: true,*/
            this.stop();
        }

    }

    //Change status of Discard Message
    changeHiddenDiscardMessage = () =>
    {
        if(this.state.isHiddenExtraTime)
        {
            this.setState({
                isHiddenPause: true,
                isHiddenPlay: false,
                isHiddenDiscard: false,
                isHiddenFinish: false,
                isHiddenDiscardMessage : true,
            });
        }
        else
        {
            this.setState({
                isHiddenPause: true,
                isHiddenPlay: true,
                isHiddenDiscard: false,
                isHiddenFinish: false,
                isHiddenDiscardMessage : true,
            });
        }
    }

    /*changeHiddenFinishMessage = () =>
    {
        this.setState({
            isHiddenFinishMessage: true,
        });
    }*/

    changeHiddenSaveSession = () =>
    {
        this.setState({
            isHiddenSaveSession: false,
        });
    }

    pauseTimer = () =>
    {
        this.setState({
            isPlaySong: false,
            isHiddenPause: true,
            isHiddenPlay: false,
            isHiddenDiscard: false,
            isHiddenFinish: false,
        });
    }

    playTimer = () =>
    {
       this.reCalculateTime();

        this.setState({
            isPlaySong: true,
            isHiddenPause: false,
            isHiddenPlay: true,
            isHiddenDiscard: true,
            isHiddenFinish: true,
        });
    }

    finishTimer = () =>
    {
        //Calculate total time in seconds.
        let totalTime = (Date.parse(new Date(this.props.newDate)) - Date.parse(new Date(this.props.olderDate))) / 1000;

        //Discomment this line if you need a confirm message for incompleted time.
        /*
        if(this.state.isHiddenExtraTime)
        {
            //If it's true, then we show a message "Are you sure you want to finish your session?"
            this.setState({isHiddenFinishMessage: false, elapsed: totalTime-this.state.diff});
        }
        else
        {
            //Pass directly another view with information.
            this.stop();
            this.setState({isHiddenFinishMessage: true, isHiddenSaveSession : false, elapsed: totalTime-this.state.diff});
        }*/

        //Stop the time.
        this.stop();
        this.setState({isHiddenSaveSession : false, elapsed: totalTime-this.state.diff});

    }

    calculateCountdown(endDate) {

        //Get time in seconds.
        let diff = (Date.parse(new Date(endDate)) - Date.parse(new Date())) / 1000;

        //Reminder
        /*if((diff % 30) == 0)
        {
            document.getElementById('play').play();
        }*/

        // clear countdown when date is reached
        if (diff < 0)
        {
            if (!isPlaySound) {
                document.getElementById('play').play();
                isPlaySound = true;
            }
            return false;
        }

        const timeLeft = {
            diff: 0,
            hours: 0,
            min: 0,
            sec: 0,
        };

        timeLeft.diff = diff;
        // calculate time difference between now and expected date
        if (diff >= 3600) { // 60 * 60
            timeLeft.hours = Math.floor(diff / 3600);
            diff -= timeLeft.hours * 3600;
        }
        if (diff >= 60) {
            timeLeft.min = Math.floor(diff / 60);
            diff -= timeLeft.min * 60;
        }
        timeLeft.sec = diff;

        return timeLeft;
    }

    calculateExtraTime(endDate)
    {
        //this.setState({isHiddenExtraTime:false});
        this.setState({
            isHiddenPause: true,
            isHiddenDiscard: false,
            isHiddenFinish: false,
            isHiddenExtraTime: false,
        });
        //console.log(endDate);
        //endDate = "2018-10-11T16:16:00";
        let extra = Math.abs((Date.parse(new Date(endDate)) - Date.parse(new Date())) / 1000);


        let extraDiff = extra;
        //console.log(extra);
        // clear countdown when date is reached
        //if (extra < 0) return false;

        const timeExtra = {
            min: 0,
            sec: 0,
        };

        // calculate time difference between now and expected date
        if (extra >= 60) {
            /*if (timeExtra.min >= 60){
                timeExtra.min = 0;
            }
            else
            {*/
                timeExtra.min = Math.floor(extra / 60);
            //}

            //extra = 0;
            extra -= Math.floor(timeExtra.min * 60);
            //extra = 0;
        }
        timeExtra.sec = extra;

        if(extraDiff > 3599)
        {
            timeExtra.min = 59;
            timeExtra.sec = 59;
            extraDiff = 3599;
            this.stop();
        }
        //return timeExtra;

        this.setState({extraMinutes: timeExtra.min, extraSeconds: timeExtra.sec,extraTime: extraDiff});
    }

    stop() {
        clearInterval(this.interval);
    }

    //Timer Design Format.
    addLeadingZeros(value) {
        value = String(value);
        while (value.length < 2) {
            value = '0' + value;
        }
        return value;
    }

    //Recalculate time when it is paused.
    reCalculateTime = () =>
    {
        let time =  JSON.parse(localStorage.getItem("time"));
        let olderDate = time.start;
        let newDate = moment(new Date()).clone().add(this.state.hours,'h').add(this.state.min,'m').add(this.state.sec,'s');

        //console.log(this.state.hours,this.state.min,this.state.sec);
        //console.log(time.end,'-',moment(newDate).clone().format("YYYY-MM-DD[T]HH:mm:ss"));
        localStorage.setItem("time",JSON.stringify({start: moment(olderDate).clone().format("MM/DD/YYYY HH:mm:ss"),end:moment(newDate).format("MM/DD/YYYY HH:mm:ss")}));
        //console.log(newDate);
    }

    render() {
        //console.log(typeof localStorage.getItem("message_status"));
        const countDown = this.state;

        if(this.props.pdf == null)
        {
            return (
                <BlackBack>
                    {this.props.song != null &&
                    <ReactPlayer url={this.props.song} width="0" height="0" playing={this.state.isPlaySong}
                                 volume={1}/>}
                    <div className="text-center">
                        <MarginTimer >
                        <TimerHours
                            title={this.state.translation['Hours']}>{this.addLeadingZeros(countDown.hours)}</TimerHours>
                        <TimerColon>:</TimerColon>
                        <TimerMinutes
                            title={this.state.translation['Minutes']}>{this.addLeadingZeros(countDown.min)}</TimerMinutes>
                        <TimerColon>:</TimerColon>
                        <TimerSeconds
                            title={this.state.translation['Seconds']}>{this.addLeadingZeros(countDown.sec)}</TimerSeconds>
                        </MarginTimer>
                        {!this.state.isHiddenExtraTime
                        &&
                        <ExtraTimeLabel>
                            +{this.addLeadingZeros(countDown.extraMinutes)}:{this.addLeadingZeros(countDown.extraSeconds)}
                        </ExtraTimeLabel>
                        }
                        <audio id='play' src={bell2}></audio>
                    </div>

                    <div className="text-center">
                        {!this.state.isHiddenDiscard &&
                        <BtnDiscart className="btn-danger" style={{"display": "inline-block"}}
                                    onClick={this.discardTimer}></BtnDiscart>}
                        {!this.state.isHiddenPlay &&
                        <BtnContinueSession className="btn-success" style={{"display": "inline-block"}}
                                            onClick={this.playTimer}></BtnContinueSession>}
                        {!this.state.isHiddenPause &&
                        <BtnPauseSession className="btn-warning" style={{"display": "inline-block"}}
                                         onClick={this.pauseTimer}></BtnPauseSession>}
                        {!this.state.isHiddenExtraTime && <span style={{margin: "70px"}}/>}
                        {!this.state.isHiddenFinish &&
                        <BtnFinishSession className="btn-default" style={{"display": "inline-block"}}
                                          onClick={this.finishTimer}></BtnFinishSession>}
                        {!this.state.isHiddenDiscardMessage &&
                        <DiscardMessage changeHidden={this.changeHiddenDiscardMessage}/>}
                        {!this.state.isHiddenSaveSession &&
                        <SaveSessionMessage elapsed={this.state.elapsed} date={this.props.olderDate}
                                            courseID={this.props.practice} extraElapsed={this.state.extraTime}/>}
                        {!this.state.isHiddenSavedMessage && <SavedMessage/>}
                    </div>
                </BlackBack>
            );
        }
        else
            {
            return (
                <BlackBack>
                    {this.props.song != null && <ReactPlayer url={this.props.song} width="0" height="0" playing={this.state.isPlaySong} volume={1} />}
                        <div className="row">
                            <div className="col-md-3">
                                <div className="text-center">
                                    <MarginTimer>
                                    <TimerHours title = {this.state.translation['Hours']}>{this.addLeadingZeros(countDown.hours)}</TimerHours>
                                    <TimerColon>:</TimerColon>
                                    <TimerMinutes title = {this.state.translation['Minutes']}>{this.addLeadingZeros(countDown.min)}</TimerMinutes>
                                    <TimerColon>:</TimerColon>
                                    <TimerSeconds title = {this.state.translation['Seconds']}>{this.addLeadingZeros(countDown.sec)}</TimerSeconds>
                                    </MarginTimer>
                                </div>

                                {!this.state.isHiddenExtraTime
                                &&
                                    <ExtraTimeLabel>
                                        +{this.addLeadingZeros(countDown.extraMinutes)}:{this.addLeadingZeros(countDown.extraSeconds)}
                                    </ExtraTimeLabel>
                                }
                                <audio id='play' src={bell2} ></audio>

                                <div className="text-center">
                                {!this.state.isHiddenDiscard && <BtnDiscart className="btn-danger" style={{"display": "inline-block"}} onClick={this.discardTimer}></BtnDiscart>}
                                {!this.state.isHiddenPlay && <BtnContinueSession className="btn-success" style={{"display": "inline-block"}} onClick={this.playTimer}></BtnContinueSession>}
                                {!this.state.isHiddenPause && <BtnPauseSession className="btn-warning" style={{"display": "inline-block"}} onClick={this.pauseTimer}></BtnPauseSession>}
                                {!this.state.isHiddenExtraTime && <span style={{margin: "70px"}} />}
                                {!this.state.isHiddenFinish && <BtnFinishSession className="btn-default" style={{"display": "inline-block"}} onClick={this.finishTimer}></BtnFinishSession>}
                                {!this.state.isHiddenDiscardMessage && <DiscardMessage changeHidden = {this.changeHiddenDiscardMessage} />}
                                {!this.state.isHiddenSaveSession && <SaveSessionMessage elapsed = {this.state.elapsed} date={this.props.olderDate} courseID = {this.props.practice} extraElapsed = {this.state.extraTime} />}
                                {!this.state.isHiddenSavedMessage && <SavedMessage />}
                                </div>
                            </div>
                            <div className="col-md-9">
                                {/*this.props.pdf != null && <iframe src={this.props.pdf+"#view=FitH&page=3"} type="application/pdf" width="100%" height="600px"/>*/}
                                {this.props.pdf != null && <FileContainer >
                                    <img src={this.props.pdf} style={{width: "100%", height: 'auto'}} />
                                    <div style={{padding: "100px", background: "#ffffff"}}></div>
                                </FileContainer>}
                            </div>
                        </div>

                </BlackBack>
            );
            }

    }
}
let isPlaySound = false;
export default CountDown;