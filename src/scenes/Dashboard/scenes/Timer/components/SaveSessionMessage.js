import React, { Component } from 'react';
import {connect} from 'react-redux';
import {createSession} from '../../../services/actionCreators';
//import MessageSaved from "./MessageSaved";
import {Congratz, CongratzIcon, CongratzTxt, TotalTime, TotalTimeLabel, AddExtraTime, BtnContainer} from '../styles/FinalSession';
import {BtnWhiteHighlighted} from '../../../styles/Buttons';
import Language from "../../../../../components/Language";
import SavedMessage from "./SavedMessage";


class SaveSessionMessage extends Component{

    constructor ()
    {
        super();

        this.state = {
            translation: Language[localStorage.getItem("language")],
            extraTime : false,
            isHiddenSavedMessage: true,
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.saveSession = this.saveSession.bind(this);

    }

    saveSession = () =>
    {

        let addSession = [];

        //If there is extra time.
        if(this.state.extraTime)
        {
          addSession = {
              access_token: localStorage.getItem("token"),
              feeling: "HAPPY",
              place: "HOME",
              course_id: this.props.courseID,
              elapsed: this.props.elapsed + this.props.extraElapsed,
              date: this.props.date
          }
        }
        else
        {
            addSession = {
                access_token: localStorage.getItem("token"),
                feeling: "HAPPY",
                place: "HOME",
                course_id: this.props.courseID,
                elapsed: this.props.elapsed,
                date: this.props.date
            }
        }

        this.props.createSession(addSession);
        this.setState({isHiddenSavedMessage: false})
    }

    handleInputChange = (event) =>
    {
        const target = event.target;
        const checked = target.checked;

        this.setState({
            extraTime: checked,
        });
    }

    secondsToHms = (d) => {
        const time = new Date(null);

        time.setSeconds(d);
        const stringTime = time.toISOString().substr(11,8);

        return stringTime;
    }

    secondsToMs = (d) => {
        const time = new Date(null);

        time.setSeconds(d);
        const stringTime = time.toISOString().substr(14,5);

        return stringTime;
    }

    render() {
        return (
            <div className="modal-delete">
                <div className="container text-center modal-color">
                    <Congratz>
                        <CongratzIcon />
                        <CongratzTxt>
                            {this.state.translation['Congratulations'] + JSON.parse(localStorage.getItem("user")).firstName}!
                        </CongratzTxt>
                    </Congratz>
                    <TotalTime>{(this.state.extraTime) ? this.secondsToHms(this.props.elapsed+this.props.extraElapsed) : this.secondsToHms(this.props.elapsed) }</TotalTime>
                    <TotalTimeLabel>Total time</TotalTimeLabel>

                    {(this.props.extraElapsed > 0) &&
                            <div className="text-center">
                        <AddExtraTime type="checkbox" checked={this.state.extraTime} value={this.props.extraElapsed} onChange={this.handleInputChange}/>
                        <label> Add extra time + {this.secondsToMs(this.props.extraElapsed)} </label>
                        </div>}
                    <BtnContainer>
                        <BtnWhiteHighlighted onClick={this.saveSession}>Save session</BtnWhiteHighlighted>
                    </BtnContainer>
                </div>
                {!this.state.isHiddenSavedMessage && <SavedMessage/>}
            </div>
        );
    }
}


const mapDispatchToProps = dispatch =>
{
    return {
        createSession (session)
        {
            dispatch(createSession(session,false));
        }
    };
}

export default connect(0,mapDispatchToProps)(SaveSessionMessage);