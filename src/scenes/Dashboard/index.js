import React, {Component} from 'react';
import GlobalStyle from '../../components/StyleTemplate';
import SideNav from './components/SideNav';
import Calendar from './scenes/Calendar/index';
import Timer from './scenes/Timer';
import store from './services/store';
import  {loadMeditationSession, loadMalaMeditationSession, loadPersonalInformation,} from './services/actionCreators';
import SideSocial from "./components/SideSocial";
import MobileNav from "./components/MobileNav";
import {SectionTitle, Phrase, TitleUnderline} from './styles/General';
import Language from "../../components/Language";
import {ClearFix} from './styles/Buttons';
import {
    ContenedorMenuBottom,
    estiloGlobal, Facebook, FraseSeccion, HeadMovil, Indicador, Instagram, LineaTitulo,
    LogotipoMenu,
    Menu01,
    Menu02,
    Menu03,
    Menu04,
    Menu05,
    Menu06,
    Menu07,
    Menu08, Menu09, MenuAyuda, MenuConfiguracion, MenuIdioma, TituloSeccion, Twitter, Txtligh,
    WraperMenuWebaap, WraperRedesIndicacdores,
    WrapperApp,
} from './styles/TemplateStyle'

export default class Dashboard extends Component{
    constructor()
    {
        super();
        this.state = {
            translation: Language[localStorage.getItem("language")],
            token: localStorage.getItem('token'),
        }

        store.dispatch(loadMeditationSession());
        store.dispatch(loadMalaMeditationSession());
        store.dispatch(loadPersonalInformation());

    }

    render() {
        document.getElementById("root").classList.add("page-scroll");
        //document.getElementById("root").setAttribute("data-simplebar","true");
        document.body.classList.add("bk-app");
        return <div>
            <WrapperApp>
                <estiloglobal/>
{/*-*************************************** Menu starts  ***************************************-*/}
                <div className="hidden-xs">
                    <WraperMenuWebaap>
                        <LogotipoMenu><img src="http://brain-think.com/tergar-app/img/icon/logotipo-tergar.svg"
                                           alt="Tergar"/></LogotipoMenu>
                        <Menu01 href="#section01"/>
                        <Menu02 href="#section02"/>
                        <Menu03 href="#section03"/>
                        <Menu04 href="#section04"/>
                        <Menu05 href="#section05"/>
                        <Menu06 href="#section06"/>
                        <Menu07 href="#section07"/>
                        <Menu08 href="#section08"/>
                        <Menu09 href="#section09"/>
                        <ContenedorMenuBottom>
                            <MenuIdioma href="#">Es</MenuIdioma>
                            <MenuAyuda/>
                            <MenuConfiguracion/>
                        </ContenedorMenuBottom>
                    </WraperMenuWebaap>

                    <WraperRedesIndicacdores className="hidden-sm">
                        <Twitter href="#" target="_blank"/>
                        <Instagram href="#" target="_blank"/>
                        <Facebook href="#" target="_blank"/>

                        <ContenedorMenuBottom>
                            <Indicador href="#"/>
                            <Indicador href="#"/>
                            <Indicador href="#"/>
                            <Indicador href="#"/>
                            <Indicador href="#"/>
                            <Indicador href="#"/>
                            <Indicador href="#"/>
                            <Indicador href="#"/>
                            <Indicador href="#"/>
                        </ContenedorMenuBottom>
                    </WraperRedesIndicacdores>
                </div>

                <HeadMovil className="visible-xs">
                    <img src="http://brain-think.com/tergar-app/img/icon/logotipo-tergar2.svg" alt="Tergar" height="40" />
                </HeadMovil>

{/*-*************************************** Menu finished  ***************************************-*/}
{/*-*************************************** Profile starts  ***************************************-*/}
                <div className="col-xs-12">
                    <TituloSeccion>Profile</TituloSeccion>
                    <FraseSeccion>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</FraseSeccion>
                    <LineaTitulo/>
                </div>
                <div>
                    {/*<ImgPerfil src="http://brain-think.com/tergar-app/img/temporal/profile-md.png" />*/}
                </div>
{/*-*************************************** Profile starts  ***************************************-*/}
                {/*Timer*/}
                <Timer/>

                <br />
                {/*Calendar*/}
                <div className="col-xs-12">
                    <TituloSeccion>PRACTICE <Txtligh>DIARY</Txtligh></TituloSeccion>
                    <FraseSeccion>A disciplined mind invites true happiness.</FraseSeccion>
                    <LineaTitulo/>
                </div>

                <Calendar/>

            </WrapperApp>

        </div>;
    }
}