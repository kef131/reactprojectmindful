import translationEN from '../lang/en.json';
import translationES from '../lang/es.json';
import translationIND from '../lang/in.json';

export default {
    "ES": translationES,
    "EN": translationEN,
    "ID": translationIND,
};
