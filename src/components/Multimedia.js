/************** PDF *****************/
import PDFNectarEN from '../img/ingles-full.jpg';
import PDFNectarES from '../img/espanol-full.jpg';

/************ Audio ****************/
import NectarES from '../audio/NectarofthePath/es/Néctar del sendero_grupo.mp3';
import NectarEN from '../audio/NectarofthePath/en/NectarofthePath.mp3';

export default {
    "ES" : {
        "Nectar of the Path" : {
            name : "Nectar of the Path",
            srcSong  : NectarES,
            srcPDF : PDFNectarES,
            durationSong: 1682,
        }
    },

    "EN" : {
        "Nectar of the Path" : {
            name: "Nectar of the Path",
            srcSong: NectarEN,
            srcPDF: PDFNectarEN,
            durationSong: 2046,
        }
    },

    "ID" : {
        "Nectar of the Path" : {
            name: "Nectar of the Path",
            srcSong: NectarEN,
            srcPDF: PDFNectarEN,
            durationSong: 2046,
        }
    }

}