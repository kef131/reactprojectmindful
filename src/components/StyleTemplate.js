
import {createGlobalStyle} from "styled-components";
import bk_body from '../img/bk/bk-body.svg';
import men_01 from '../img/icon/men-01.svg';
import men_01a from '../img/icon/men-01a.svg';
import men_02 from '../img/icon/men-02.svg';
import men_02a from '../img/icon/men-02a.svg';
import men_03 from '../img/icon/men-03.svg';
import men_03a from '../img/icon/men-03a.svg';
import men_04 from '../img/icon/men-04.svg';
import men_04a from '../img/icon/men-04a.svg';
import men_05 from '../img/icon/men-05.svg';
import men_05a from '../img/icon/men-05a.svg';
import men_06 from '../img/icon/men-06.svg';
import men_06a from '../img/icon/men-06a.svg';
import men_07 from '../img/icon/men-07.svg';
import men_07a from '../img/icon/men-07a.svg';
import men_08 from '../img/icon/men-08.svg';
import men_08a from '../img/icon/men-08a.svg';
import men_09 from '../img/icon/men-09.svg';
import men_09a from '../img/icon/men-09a.svg';
import men_10 from '../img/icon/men-10.svg';
import men_10a from '../img/icon/men-10a.svg';
import men_11 from '../img/icon/men-11.svg';
import men_11a from '../img/icon/men-11a.svg';
import men_12 from '../img/icon/men-12.svg';
import men_12a from '../img/icon/men-12a.svg';
import logout from '../img/icon/logout.png';
import ico_duracion from '../img/icon/ico-duracion.svg';
import ico_cuentas from '../img/icon/ico-cuentas.svg';
import emotion1a from '../img/icon/emotion1a.svg';
import emotion1b from '../img/icon/emotion1b.svg';
import emotion2a from '../img/icon/emotion2a.svg';
import emotion2b from '../img/icon/emotion2b.svg';
import emotion3a from '../img/icon/emotion3a.svg';
import emotion3b from '../img/icon/emotion3b.svg';
import emotion4a from '../img/icon/emotion4a.svg';
import emotion4b from '../img/icon/emotion4b.svg';
import place1 from '../img/icon/place1.svg';
import place1b from '../img/icon/place1b.svg';
import place2 from '../img/icon/place2.svg';
import place2b from '../img/icon/place2b.svg';
import place3 from '../img/icon/place3.svg';
import place3b from '../img/icon/place3b.svg';
import place4 from '../img/icon/place4.svg';
import place4b from '../img/icon/place4b.svg';

const GlobalStyle = createGlobalStyle`
// @charset "UTF-8";
// /* CSS Document */
// html,body{height: 100%; font-family: 'Roboto', sans-serif; font-weight: 400; color:#727070; font-size: 13px;}
// .bk-app{background: url(${bk_body}) no-repeat bottom center fixed; background-size: contain;}
// .page-scroll{height: 100%; width: calc(100% - 90px); padding-left: 16px; margin-left: 45px;}
// .cont-sombra-redondeado{box-shadow: 0 0 16px rgba(0,0,0,.25); border-radius: 8px; overflow: hidden; background: rgba(255,255,255,.90)}
//
// /************************************************************ SCROLL ************************************************************/
// .simplebar-scrollbar::before {background: #b8b8b8 !important;}
// .simplebar-track .simplebar-scrollbar.visible::before {opacity: 0.95 !important;}
// .simplebar-scrollbar {width: 5px !important;}
//
// /************************************************************ MENU ************************************************************/
// .sidenav {height: 100%; width: 18px; position: fixed; z-index: 3; top: 0; left: 16px; transition: 0.5s; padding-top: 16px;}
// .logo-tergar{height: 40px;margin-left: -11px;}
// .cont-menu a, .opcion-extra a{margin-bottom: 24px; display: block; height: 18px; background-repeat: no-repeat; background-size: contain; background-position: center; }
// .opcion-extra{width: 100%; position: absolute; bottom: 0;}
// .menu01{background-image: url(${men_01});}    .menu01:hover, .menu01:focus, .seleccionado01, .active .menu01{background-image: url(${men_01a});}
// .menu02{background-image: url(${men_02});}    .menu02:hover, .menu02:focus, .seleccionado02, .active .menu02{background-image: url(${men_02a});}
// .menu03{background-image: url(${men_03});}    .menu03:hover, .menu03:focus, .seleccionado03, .active .menu03{background-image: url(${men_03a});}
// .menu04{background-image: url(${men_04});}    .menu04:hover, .menu04:focus, .seleccionado04, .active .menu04{background-image: url(${men_04a});}
// .menu05{background-image: url(${men_05});}    .menu05:hover, .menu05:focus, .seleccionado05, .active .menu05{background-image: url(${men_05a});}
// .menu06{background-image: url(${men_06});}    .menu06:hover, .menu06:focus, .seleccionado06, .active .menu06{background-image: url(${men_06a});}
// .menu07{background-image: url(${men_07});}    .menu07:hover, .menu07:focus, .seleccionado07, .active .menu07{background-image: url(${men_07a});}
// .menu09{background-image: url(${men_09});}    .menu09:hover, .menu09:focus, .seleccionado09, .active .menu09{background-image: url(${men_09a});}
// .menu08{background-image: url(${men_08});}    .menu08:hover, .menu08:focus, .seleccionado08, .active .menu08{background-image: url(${men_08a});}
// .menu10{background-image: url(${men_10});}    .menu10:hover, .menu10:focus, .seleccionado10, .active .menu10{background-image: url(${men_10a});}
// .menu11{background-image: url(${men_11});}    .menu11:hover, .menu11:focus, .seleccionado11, .active .menu11{background-image: url(${men_11a});}
// .menu12{background-image: url(${logout}); cursor:pointer;}    .menu12:hover, .menu12:focus, .seleccionado12, .active .menu12{background-image: url(${logout}); cursor:pointer;}
//
// .btn-idioma{font-size: 8px; font-weight: 400; color: #5c6bc0; text-align: center; background-image:url(${men_12}); padding-top: 1px;} 
// .btn-idioma:hover{color:#FF9333; text-decoration: none; background-image:url(${men_12a});}
//
// .navbar{width: 100%; background:rgba(255,255,255,0.90);min-height:10px; border: none; box-shadow: 0 0 10px rgba(150,150,150,.3);}
// .navbar-brand {height: auto;padding: 5px 15px;}
// .navbar-nav > li > a {background-repeat: no-repeat;}
// .navbar-inverse .navbar-nav > li > a:hover, .navbar-inverse .navbar-nav > li > a:focus{text-decoration: none;background-color:rgba(73,83,170,0.05); color: #ff9333;}
// .navbar-toggle {border: 1px solid #5c6bc0 !important; margin-top: 6px; margin-bottom: 6px;}
// .navbar-inverse .navbar-toggle .icon-bar {background-color: rgba(92,107,192,1);}
// .navbar-inverse .navbar-toggle:hover, .navbar-inverse .navbar-toggle:focus {background: rgba(92,107,192,.20);}
// .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus {color: #ff9333;background-color: rgba(73,83,170,0.05);}
// .navbar-header {float: none;border-bottom: 1px solid #e8e8e8;}
// .navbar-toggle {display: block;}
// .navbar-collapse.collapse {display: none!important;}
// .navbar-nav {float: none!important;}
// .navbar-nav>li {float: none;}
// .navbar-collapse.collapse.in{display:block !important;}
// .navbar-fixed-top .navbar-collapse, .navbar-fixed-bottom .navbar-collapse {max-height:800px;}
// .navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form {border-color:transparent;}
// /************************************************************ MENU ************************************************************/
//
//
// /************************************************************ REDES INDICADORES ************************************************************/
// .side-redes {height: 100%; width: 48px; position: fixed;  top: 0; right: 0; overflow-x: hidden; transition: 0.5s; text-align: center; padding: 0 16px;}
// .side-redes a{margin: 16px 0; display: block;}
// .indicadores{position: absolute;bottom: 0; left: 50%; margin-left: -3px;}
// .indicadores a{width: 6px; height: 6px; background:#b8b8b8; margin:20px 0 ; border-radius: 20px;}
// .indicador-activo, .indicadores a:hover{background: #ff9333 !important;}
// /************************************************************ REDES INDICADORES ************************************************************/
//
//
// /************************************************************ TITULOS ************************************************************/
// h2{font-size: 26px; color:#5c6bc0; font-weight: 400; margin-top: 20px; margin-bottom: 5px;}
// h2 span{font-weight: 400;}
// h3{font-size:16px; font-weight: 400; font-style: italic; margin: 0;}
// .line-titulo{background: #5c6bc0; width: 100px; height: 2px; margin: 10px 0 0;}
// h4{font-weight: 400; margin: 0; font-size: 18px;}
// /************************************************************ TITULOS ************************************************************/
//
// /************************************************************ MARGENES ************************************************************/
// .mt16{margin-top: 16px;}
// .mt32{margin-top: 32px;}
// .mb16{margin-bottom: 16px;}
// .mb32{margin-bottom: 32px;}
// .pt40{padding-top: 40px;}
// /************************************************************ MARGENES ************************************************************/
//
// .head-naranja{background: #ff9333; color: #fff; overflow: hidden; padding: 16px 17px; height: 67px;}
// .indicador-anio a{width: 10px; display: inline-block; vertical-align: middle;}
// .indicador-anio a:hover, .indicador-anio a:focus{opacity: .75;}
// .indicador-anio p{ display: inline-block; vertical-align: middle; font-size: 30px; margin: 0 4px; line-height: 34px;}
//
// .filtrar-calendario{margin-top: 0px;}
// label{color:#2F3D6E; font-size: 12px; font-style: italic; font-weight: 400; margin: 0;}
// .filtrar-calendario label{padding-right: 5px;}
// .scroll-listado-detalle label{padding-left: 5px;}
// .dropdown > .btn{padding: 6px;}
// .filtrar-calendario .dropdown > button {width: 80%;}
// .filtrar-calendario .dropdown > .dropdown-menu{right: 0; left:20%; color:#727070; font-weight: 400;  font-size: 13px; overflow: hidden;}
// .dropdown > button {width: 100%; text-align: left; border-top: 0; border-right: 0; border-left: 0; border-radius: 0; border-color: #b8b8b8; color:#727070; font-weight: 400; font-size: 13px;}
// .dropdown > button:hover, btn-default:hover, .btn-default:focus, .btn-default.focus, .btn-default:active, .btn-default.active, .open > .dropdown-toggle.btn-default {color: #727070; border-color: #b8b8b8;background: #ffffff;}
// .dropdown > button > span{float: right; margin-top: 8px;}
// .dropdown > .dropdown-menu{right: 0; left:0; color:#727070; font-weight: 400;  font-size: 13px;}
// .dropdown > .dropdown-menu > li > a{color:#727070; font-weight: 400;}
// .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {color: #262626;background-color: rgba(224,233,255,0.5);}
// .input-group .form-control:first-child, .input-group-addon:first-child, .input-group-btn:first-child > .btn, .input-group-btn:first-child > .btn-group > .btn, .input-group-btn:first-child > .dropdown-toggle, .input-group-btn:last-child > .btn:not(:last-child):not(.dropdown-toggle), .input-group-btn:last-child > .btn-group:not(:last-child) > .btn {border-radius: 0; border-top: none; border-left: none;border-right: none;}
// .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {background:#fff; box-shadow: none;}
// .input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:last-child > .btn, .input-group-btn:last-child > .btn-group > .btn, .input-group-btn:last-child > .dropdown-toggle, .input-group-btn:first-child > .btn:not(:first-child), .input-group-btn:first-child > .btn-group:not(:first-child) > .btn {border-radius: 0;border-top: none; border-left: none;border-right: none;}
// .timepicker input, .timepicker input:focus, .timepicker input:active{box-shadow: none;}
// .form-control, .form-control:focus, .form-control:active{box-shadow: none; border-radius: 0; border-top: none; border-right: none; border-left: none; padding: 6px; height: auto; font-size: 13px; border-color:#b8b8b8;}
// .input-group-addon {border-color:#b8b8b8;} 
// .solo-lectura{background: rgba(231,231,231,0.90);} 
// .signo{color:#5c6bc0; text-align: center; padding: 24px 0 0; font-weight: 400; width: 6.46%}
// .input-group-addon {background: #fff;}
// .scroll-listado-detalle .glyphicon{color:#b8b8b8;}
//
// .meses{padding: 0; border-bottom: 1px solid #b8b8b8; list-style: none; margin: 0;}
// .meses li{float:left; width: 8.33%; text-align: center;}
// .meses li a{color: #727070; border-bottom: 3px solid #fff; transition: all 0.3s ease;}
// .mes-activo, .meses li a:hover{border-bottom: 3px solid #ff9333 !important; color:#ff9333 !important; display: inline-block;padding: 0 0 6px; text-decoration: none;}
// .body-calendario{padding: 32px; height: 532px;}
// .body-listados-detalle{padding: 0 32px; height: 532px;}
// .body-calendario th, .body-calendario td{text-align: center; padding: 16px;}
// .body-calendario th{font-size: 20px; color:#5C6BC0; font-weight: 400;}
// .body-calendario td{font-size: 18px; font-weight: 400; position: relative; overflow: hidden;}
// .dia-desactivado{color:#B8B8B8;}
//
// .scroll-listado-detalle{height: 100%;}
// .fecha-head{line-height: 10px; line-height: 14px; width: 100px; float: right; margin: 0;}
// .fecha-head span{font-size:25px; line-height: 24px;}
// .listado-practicas{border-bottom: 1px solid #b8b8b8; padding-bottom: 16px; margin-bottom: 24px; cursor:pointer;}
// .listado-practicas h4{overflow: hidden; margin-bottom: 4px; height: 20px;}
// .listado-practicas img{width: 18px; height: 18px; margin: 2px;}
// .duracion, .malas-cuentas{color:#B8B8B8; font-size: 12px; padding-left: 20px; background-position: left center; background-repeat: no-repeat; display: inline-block; vertical-align: middle; margin-bottom: 4px;}
// .duracion{background-image: url(${ico_duracion}); margin-right: 30px;}
// .malas-cuentas{background-image: url(${ico_cuentas});}
// .campo-nota-practica textarea{width: 100%; padding: 6px; resize: none; border:1px solid #b8b8b8; height: 80px;}
// .campo-nota-practica textarea::placeholder{font-style: italic;}
// .estado-animo-lugares span{display: table-cell; padding-right: 26px; text-align: center; padding-top: 6px;}
// .estado-animo-lugares a{width: 50px; background-position: top center; background-repeat: no-repeat; background-size: contain; padding-top: 54px;  color:#b8b8b8; text-decoration: none; display: block;}
// .estado-animo-lugares a:hover, .estado-animo a:focus{text-decoration: none; color: #ff9333;}
// .scroll-animo-lugares{width: 100%;}
//
// .edo-animo01{background-image: url(${emotion1b});} 	.edo-animo01:hover, .edo-animo01:focus, .edo-animo01-activo{background-image: url(${emotion1a});}
// .edo-animo02{background-image: url(${emotion2b});} 	.edo-animo02:hover, .edo-animo02:focus, .edo-animo02-activo{background-image: url(${emotion2a});}
// .edo-animo03{background-image: url(${emotion3b});} 	.edo-animo03:hover, .edo-animo03:focus, .edo-animo03-activo{background-image: url(${emotion3a});}
// .edo-animo04{background-image: url(${emotion4b});} 	.edo-animo04:hover, .edo-animo04:focus, .edo-animo04-activo{background-image: url(${emotion4a});}
//
// .lugar-pactica01{background-image: url(${place1b});} 	.lugar-pactica01:hover, .lugar-pactica01:focus, .lugar-pactica01-activo{background-image: url(${place1});}
// .lugar-pactica02{background-image: url(${place2b});} 	.lugar-pactica02:hover, .lugar-pactica02:focus, .lugar-pactica02-activo{background-image: url(${place2});}
// .lugar-pactica03{background-image: url(${place3b});} 	.lugar-pactica03:hover, .lugar-pactica03:focus, .lugar-pactica03-activo{background-image: url(${place3});}
// .lugar-pactica04{background-image: url(${place4b});} 	.lugar-pactica04:hover, .lugar-pactica04:focus, .lugar-pactica04-activo{background-image: url(${place4});}
//
//
// .existe-sesion span, .dia-seleccionado span{position: relative; z-index: 2; cursor:pointer;}
// .existe-sesion::before, .dia-seleccionado::before {content:' '; border-radius:150px; background: rgba(92,107,192,0.10); width: 44px; height: 44px; display: inline-block; position: absolute;top: 50%;margin-top: -22px;left: 50%;margin-left: -22px;}
// .dia-seleccionado::before{background: rgba(92,107,192,1);}
// .dia-seleccionado{ color:#fff;}
// .racha-inicio::after {content:' '; border-radius:60px 0 0 60px; background: rgba(92,107,192,0.10); width: 100%; height: 44px; display: inline-block; position: absolute;top: 50%;margin-top: -22px; left: 50%; margin-left: -22px;}
// .racha-inicio-finsemana::after {content:' '; border-radius:60px 4px 4px 60px; background: rgba(92,107,192,0.10); width: 44px; height: 44px; display: inline-block; position: absolute;top: 50%;margin-top: -22px; left: 50%; margin-left: -22px;}
// .racha-inicio-intermedia::after {content:' '; border-radius:4px 0 0 4px; background: rgba(92,107,192,0.10); width: 100%; height: 44px; display: inline-block; position: absolute;top: 50%;margin-top: -22px; left: 50%; margin-left: -22px;}
// .racha-intermedia::after{content:' '; background: rgba(92,107,192,0.10); width: 100%; height: 44px; display: inline-block; position: absolute;top: 50%;margin-top: -22px; left:0; }
// .racha-fin-intermedia::after {content:' '; border-radius:0 4px 4px 0; background: rgba(92,107,192,0.10); width: 100%; height: 44px; display: inline-block; position: absolute;top: 50%;margin-top: -22px; right: 50%; margin-right: -22px;}
// .racha-fin::after {content:' '; border-radius:0 60px 60px 0; background: rgba(92,107,192,0.10); width: 100%; height: 44px; display: inline-block; position: absolute;top: 50%;margin-top: -22px; right: 50%; margin-right: -22px;}
// .racha-fin-iniciosemana::after {content:' '; border-radius:0 60px 60px 0; background: rgba(92,107,192,0.10); width: 44px; height: 44px; display: inline-block; position: absolute;top: 50%;margin-top: -22px; right: 50%; margin-right: -22px;}
// /************************************************************ CALENDARIO ************************************************************/
//
// /************************************************************ BOTONES ************************************************************/
// button{border-radius: 3px; border-style: solid; border-width: 1px; background-color: transparent; padding: 0 15px 0 10px; transition: all 0.3s ease;}
// button img, button hr, button span{display: inline-block; vertical-align: middle;}
// button img{width: 11px; margin-right: 10px;} button hr{width: 1px; height: 32px; margin: 0; margin-right: 12px;}
//
// .agregar-sesion{border-color: #fff; } .agregar-sesion hr{background-color: #fff; } .agregar-sesion:hover,.agregar-sesion:focus{background-color: rgba(255,255,255,0.10)}
// .regresar-practicas{border-color: #fff; } .regresar-practicas hr{background-color: #fff; } .regresar-practicas:hover,.regresar-practicas:focus{background-color: rgba(255,255,255,0.10)}
// .guardar{border-color:#ff9333; color: #ff9333;} .guardar hr{background-color: #ff9333; } .guardar:hover,.guardar:focus{background-color: rgba(255,147,51,0.10)}
// .eliminar{border-color:#5C6BC0; color: #5C6BC0;} .eliminar hr{background-color: #5C6BC0; } .eliminar:hover,.eliminar:focus{background-color: rgba(92,107,192,0.10)}
// /************************************************************ BOTONES ************************************************************/
//
// .existe-sesion, .dia-seleccionado{padding: 2px;}
// .existe-sesion span{border-radius: 50px; background: rgba(92,107,192,0.10); padding: 12px;}
// .dia-seleccionado span{border-radius: 50px; background: rgba(92,107,192,1); padding: 12px; color:#fff;}
//
// /************************************************************ MENSAJES ************************************************************/
// .mensajes{padding: 24px 30px 20px; overflow: hidden; }
// .mensajes h2{font-size: 18px;}
// /************************************************************ MENSAJES ************************************************************/
//
// .modal-delete {
//     display: block; /* Hidden by default */
//     position: fixed; /* Stay in place */
//     z-index: 999; /* Sit on top */
//     left: 0;
//     top: 0;
//     overflow: hidden;
//     width: 100%; /* Full width */
//     height: 100%; /* Full height */
//     background-color: rgb(0,0,0); /* Fallback color */
//     background-color: rgba(0,0,0,1); /* Black w/ opacity */
// }
//
// .modal-color {
// position: absolute;
//     width: 400px;
//     height: 300px;
//     top: 50%;
//     margin-top: -150px;
//     left: 50%;
//     margin-left: -200px;
// }
// /************************************************************ VARIOS ************************************************************/
// .campo50{width: 46.5%;}
// .campo30{width: 29%;}
// .f-left{float:left;}
// .f-right{float:right;}
// .font16{font-size: 16px;}
// .text-center{text-align: center !important;}
//
// /************************************************************ VARIOS ************************************************************/
//
//
// /******************************************************** ESTILOS MEDIA QUERY RESPONSIVE  ********************************************************/
// @media  (max-width: 1199px){
// }
//
// @media  (max-width: 1024px){
// 	h4{font-size: 16px;}
//	
// 	.page-scroll {height: 100%;width: calc(100% - 48px);}
// 	.head-naranja {padding: 16px 5px;}
// 	.body-calendario{padding: 32px 20px; height: 454px;}
// 	.body-listados-detalle{padding: 0 20px; height: 454px;}
// 	.side-redes{display:none;}
// 	.body-calendario th, .body-calendario td {text-align: center; padding: 12px; font-size: 16px;}
// 	.ocultar{display: none;}
// }
//
// @media  (max-width: 991px){
// 	.sidenav {display: none;}
// 	.page-scroll {height: 100%;width: 100%;margin: 0;padding: 0;}
// 	.navbar-nav > li > a {padding-left: 40px;background-size: 20px;background-position: 10px 12px;}
// 	.body-calendario {padding: 0 20px 32px; height: 360px;}
// 	.body-listados-detalle {height: 392px;}
// 	.navbar-nav {background: #fff;}
//
// }
//
// @media  (max-width: 767px){
// 	h2 {font-size: 20px;}
// 	.mt32{margin-top: 24px;}
//	
// 	.indicadores a{margin:15px 0;}
// 	.filtrar-calendario label {padding-right: 0px; float: left; padding-top: 7px;}
// 	.filtrar-calendario .dropdown > button {width: 83%; float: right;}
// 	.filtrar-calendario {margin-top: 10px;}
// 	.agregar-sesion{padding: 5px 8px}
// 	.agregar-sesion hr, .agregar-sesion span{display: none;}
// 	.agregar-sesion img{margin: 0;}
// 	.body-calendario th {padding: 0px 5px 10px;}
// 	.body-calendario td{font-size: 13px; padding: 11px 5px;}
// 	.body-calendario {padding: 0 20px 24px; height: 300px;}
// 	.indicador-anio p {font-size: 18px;}
// 	.head-naranja {padding: 12px 5px;height: 58px;}
// 	.fecha-head span {font-size: 20px; line-height: 20px;}
//
// }
//
//
// @media screen and (max-height: 795px) {
// 	.opcion-extra{position: relative;}
// }
//
// @media screen and (max-height: 280px) {
// 	.indicadores{display: none;}
// }

`;

export default GlobalStyle;
