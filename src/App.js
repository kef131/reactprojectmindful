import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import Login from './scenes/Login';
import Index from './scenes/Register';
import RecoverPassword from './scenes/Password/RecoverPassword';
import ResetPassword from './scenes/Password/ResetPassword';
import Dashboard from './scenes/Dashboard/';
import Language from "./components/Language";
import {Provider} from 'react-redux';
import store from './scenes/Dashboard/services/store';
import {
    loadMalaMeditationSession,
    loadMeditationSession,
    loadPersonalInformation
} from "./scenes/Dashboard/services/actionCreators";
// import {
//     BtnResaltadoBco,
//     BtnBcoCalendar
// } from './style/BtnGeneral'


export default class App extends Component {

    constructor()
    {
        super();

        //If local language doesn't exists, save language currently in local storage.
        if (!("language" in localStorage))
        {
            //If local language doesn't exist in component Language, by default will be English.
            (navigator.language.split('-')[0].toUpperCase() in Language) ? localStorage.setItem("language",navigator.language.split('-')[0].toUpperCase()): localStorage.setItem("language","EN");
        }
        else
        {
            console.log(navigator.language.split('-')[0].toUpperCase());
            (navigator.language.split('-')[0].toUpperCase() in Language) ? localStorage.setItem("language",navigator.language.split('-')[0].toUpperCase()): localStorage.setItem("language","EN");
        }

    }

    requiredAuth()
    {
        return ("token" in localStorage);
    }

    render() {
        if("token" in localStorage)
        {
            store.dispatch(loadMeditationSession());
            store.dispatch(loadMalaMeditationSession());
            store.dispatch(loadPersonalInformation());
        }
    return (

        <Router basename={process.env.BASE_URL} >
            <div>
                <Switch>
                    <Route exact path="/" render={() => <Redirect to="/dashboard"/> } />
                    <Route exact path="/login" component={Login} />
                    <Route path="/register" component={Index}/>
                    <Route path="/recover_password" component={RecoverPassword}/>
                    <Route path="/reset_password/:token" render = {(props) => <ResetPassword {...props} />}/>
                    <Provider store={store}>
                    <Route path="/dashboard" render={ () => this.requiredAuth() ? <Dashboard/>: <Redirect to ="/login"/>} />
                    </Provider>

                </Switch>
            </div>
        </Router>
    );
  }
}